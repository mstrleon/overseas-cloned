$Id

-- SUMMARY --

Floating Manager Menu is intended as an alternative to such modules as Simple Menu and Administrator Menu
with a focus on non-administrative content editors and managers. It allows you to include an unobtrusive
and easily styled menu without the need to dedicate space within your site's theme. 

-- INTEGRATION --

* DHTML Menu:
  If you are using DHTML menu for javascript menus that do not require page reloads, you will experience
  out-of-the-box support with Floating Manager Menu.

-- INSTALLATION --

* Copy floating_manager_menu to your modules directory and enable it on the admin modules page.

-- CONFIGURATION --

* Begin by granting view access to appropriate roles on the admin permissions page.

* Manage which menu to include, themes, default positioning, and admin page exclusion at 
  admin/structure/menu/floating_manager_menu

-- CUSTOMIZATION --

Custom appearance is supported through CSS skins placed in the `skins` directory.

See any of the three included skins for an example.

-- FAQ --

Q: Why doesn't the menu follow the page in Safari/Chrome/WebKit?

A: WebKit browsers have problems with jqueryui.draggable and fixed positioning. We can do draggable or we can 
    do fixed positioning, but right now we can't do both. This is something I will monitor and if WebKit starts
    to behave eventually, I'll remove that restriction.
    
    A potential alternative to this in the meantime would be to allow site administrators to choose whether to 
    degrade in favour of fixed positioning or draggable.


-- CONTACT --

Current maintainer:
* Joshua Schroeder (jdschroeder) - http://drupal.org/user/202642
* Drupal 7 port contributed by cmcs - http://drupal.org/user/387667

This project has been sponsored by:
* The University of Lethbridge
  Visit http://www.uleth.ca for more information.

* Redwall Studios
  Southern Alberta's Drupal specialists. Visit http://redwall.ca for more information.