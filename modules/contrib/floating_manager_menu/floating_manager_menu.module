<?php

/**
 * @file
 * Floating Manager Module, a jQuery draggable menu for conent managers
 *
 * For times when you want to give your content managers a simple and unobtrusive menu that
 * may not fit into your site's theme, Floating Manager Module will creating a floating and
 * collapsable menu.
 *
 * @author: Joshua Schroeder (jdschroeder) <http://drupal.org/user/202642>
 */

/**
 * Implementation of hook_permission().
 */
function floating_manager_menu_permission() {
  return array(
    'view floating manager menu' => array('title' => 'View Floating Manager'),
    'administer floating manager menu'  => array('title' => 'Administer Floating Manager'),
  );
}

/**
 * Implementation of hook_menu().
 */
function floating_manager_menu_menu() {
  
  // This callback is how we will set persistent positioning via AJAX.
  // The 2, 3, and 4 arguments represent top position, left position, and display status, respectively.
  $items['floatingmanagermenu/positioning_set/%/%/%'] = array(
    'title' => 'Floating Menu persistent settings',
    'description' => 'Floating Menu persistent settings',
    'page callback' => 'floating_manager_menu_positioning_set',
    'page arguments' => array(2, 3, 4),
    'access arguments' => array('view floating manager menu'),
    'type' => MENU_CALLBACK,
  );
  
  $items['admin/structure/menu/floating_manager_menu'] = array(
    'title' => 'Floating Manager Menu',
    'description' => 'Floating Manager Menu Settings',
    'page callback' => 'floating_manager_menu_settings',
    'access arguments' => array('administer floating manager menu'),
    'type' => MENU_LOCAL_TASK,
  );
    
  return $items;
}

/**
 * Implemention of hook_help().
 */
function floating_manager_menu_help($path, $arg) {
  switch ($path) {
    case 'admin/settings/floating_manager_menu':
      return t('Floating Manager Menu provides a user friendly menu for adding and moderating content without having to provide administrative access to your moderators and content editors.');
      
    case 'admin/help#floating_manager_menu':
      return t('Floating Manager Menu provides a user friendly menu for adding and moderating content without having to provide administrative access to your moderators and content editors.');
      
  }
}

/**
 * Implementation of hook_init().
 */
function floating_manager_menu_init() {
  define('FLOATING_MANAGER_MENU_SKIN', variable_get('floating_manager_menu_skin', 'default'));  
  if (user_access('view floating manager menu') && (!floating_manager_menu_admin_hide())) {
    drupal_add_library('system','ui.draggable');
    drupal_add_css(drupal_get_path('module', 'floating_manager_menu') . '/skins/' . FLOATING_MANAGER_MENU_SKIN . '/floating_manager_menu.css');
    drupal_add_js(drupal_get_path('module', 'floating_manager_menu') . '/js/floating_manager_menu.js');
  }
}

/**
 * Implementation of hook_page_alter().
 */
function floating_manager_menu_page_alter(&$page) { 
  $page['page_bottom']['floating_manager_menu'] = array(
    '#markup' => _floating_manager_menu_markup(), 
    '#weight' => 25
  ); 
}

/**
 * Display the output markup for the floating menu
 */
function _floating_manager_menu_markup() {

  $return = '';

  // This is where we actually show the menu, assuming we have permissions and aren't hiding it.
  if (user_access('view floating manager menu') && (!floating_manager_menu_admin_hide())) {

    $styles = floating_manager_menu_positioning_css();
    $return = '<div id="floating-manager-menu"' . $styles[0] . '>
    	<img src="' . base_path() . drupal_get_path('module', 'floating_manager_menu') . '/images/spacer.gif" alt="Minimize menu" class="close-button" onclick="javascript:togglefmm();" /><h2>' . t('Site Manager Menu') . 
    	  '</h2><div class="inner" ' . $styles[1] . '>';
    // Retrieve the appropriate menu from the Menu module.
    // If we are using DHTML Menu, it will already be nicely formatted for us.
    $menu_tree = menu_tree(variable_get('floating_manager_menu_menu', 'navigation'));
    $return .= drupal_render($menu_tree);
    $return .= '</div></div>';

  }

  return $return;
}

/**
 * Callback for administrative settings form.
 *
 * Form that allows skin and menu selection, among other admin-configurable options
 *
 * @return
 *   Formatted HTML form.
 */
function floating_manager_menu_settings() {
  $return = drupal_get_form('floating_manager_menu_settings_form');
  return $return;
}

/**
 * Defines administrative settings form.
 *
 * All settings for this module should be managed through this form.
 *
 * @return
 *   Array of Form API elements.
 */
function floating_manager_menu_settings_form() {
  
  // Add Farbtastic color picker
  drupal_add_css('misc/farbtastic/farbtastic.css', 'module', 'all', FALSE);
  drupal_add_js('misc/farbtastic/farbtastic.js');
  drupal_add_js(drupal_get_path('module', 'floating_manager_menu') . '/js/floating_manager_menu_admin.js');
  drupal_add_css(drupal_get_path('module', 'floating_manager_menu') . '/css/floating_manager_menu_admin.css');
  
  $form['floating_manager_menu_menu'] = array(
    '#title' => t('Menu to display'),
    '#description' => t('Select which system menu to use for your manager menu. Default is the Navigation menu.'),
    '#type' => 'select',
    '#options' => menu_get_menus(TRUE),
    '#required' => TRUE,
    '#default_value' => variable_get('floating_manager_menu_menu', 'navigation'),
  );
  
  $form['floating_manager_menu_skin'] = array(
    '#title' => t('Skin'),
    '#description' => t('Skins can be found and added in the floating_manager_menu/skins folder'),
    '#type' => 'select',
    '#options' => floating_manager_menu_skin_list(),
    '#required' => TRUE,
    '#default_value' => variable_get('floating_manager_menu_skin', 'default'),
  );
  
  $form['defaultposition'] = array(
    '#title' => t('Default position'),
    '#type' => 'fieldset',
    '#description' => t('If the user has not positioned the menu, this will be its starting point. Specify a numeric pixel or percentage value as you would use for an absolutely positioned CSS element. (example: 50 or 10%)'),
  );
  
  $form['defaultposition']['floating_manager_menu_top_default'] = array(
    '#title' => 'Top',
    '#type' => 'textfield',
    '#size' => 3,
    '#required' => TRUE,
    '#suffix' => 'pixels',
    '#default_value' => variable_get('floating_manager_menu_top_default', ''),
  );
  
  $form['defaultposition']['floating_manager_menu_left_default'] = array(
    '#title' => 'Left',
    '#type' => 'textfield',
    '#size' => 3,
    '#required' => TRUE,
    '#suffix' => 'pixels',
    '#default_value' => variable_get('floating_manager_menu_left_default', ''),
  );
  
  $form['floating_manager_menu_adminexclude'] = array(
    '#title' => t('Exclude from admin pages'),
    '#description' => t('Check this option if you want to exclude the menu from admin pages'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('floating_manager_menu_adminexclude', 0),
  );
  
  // Encourage people to use in conjuntion with DHTML Menu, because it makes this module so much cooler.
  $form['dhtml'] = array(
    '#value' => '<p>' . t('Power Tip: Try enabling !module to enjoy Javascript menus, reducing the number of page loads when using nested menus.', 
      array('!module' => l('DHTML Menu', 'http://drupal.org/project/dhtml_menu'))) . '</p>',
    '#type' => 'markup',
  );

  return system_settings_form($form);
  
}


/**
 * Validate settings form results.
 *
 * Ensure that positioning values are numeric.
 *
 * @return
 *   NULL
 */
function floating_manager_menu_settings_form_validate($form_id, &$form_state) {
  if ($form_id == 'floating-manager-menu-settings-form') {
    if (!is_numeric($form_state['values']['floating_manager_menu_top_default'])) {
      form_set_error('floating_manager_menu_top', t('Please enter a numeric value for the top value.'));
    }
    if (!is_numeric($form_state['values']['floating_manager_menu_left_default'])) {
      form_set_error('floating_manager_menu_left', t('Please enter a numeric value for the left value.'));
    }
  }
  
  // The scheme is just used to populate fields in the form and we don't need to save it to the database
  unset($form_state['values']['scheme']);
  
  return;
}

/**
 * Callback for saving the current position of the menu.
 *
 * AJAX call will use this callback to save sessional variables to permit persistent positioning and collapse state.
 *
 * @param $top
 *   CSS value for current top property.
 *
 * @param $left
 *   CSS value for current left property.
 *
 * @param $collapsed
 *   CSS value for current display property.
 *
 * @return
 *   NULL
 */
function floating_manager_menu_positioning_set($top = NULL, $left = NULL, $collapsed = 'block') {
  
  // The regular expressions here are to remove the 'px' that may appear after our numeric value.
  $_SESSION['fmmtop'] = preg_replace('/([\d]*)(.*)/', '$1', $top);
  $_SESSION['fmmleft'] = preg_replace('/([\d]*)(.*)/', '$1', $left);
  $_SESSION['fmmcollapsed'] = $collapsed;
  
  return;
}

/**
 * Generate inline CSS to position based on saved sessionals.
 *
 * Retrieve sessional variables if they exist so that we can persistently position the menu.
 *
 * @return
 *   Array of strings to insert into #floating-manager-menu and #floating-manager-menu .inner div tags
 */
function floating_manager_menu_positioning_css() {
  // Get positions from the session if they exist, otherwise use our defined defaults
  $top = (isset($_SESSION['fmmtop'])) ? $_SESSION['fmmtop'] : variable_get('floating_manager_menu_top_default', 0);
  $left = (isset($_SESSION['fmmleft'])) ? $_SESSION['fmmleft'] : variable_get('floating_manager_menu_left_default', 0);

  // Get collapse status from the session if it exists, otherwise default to showing it.
  // We also want to add a class to the overall div to indicate collapsed status. We'll use $collapsed for that.
  $blockstyle = (isset($_SESSION['fmmcollapsed'])) ? $_SESSION['fmmcollapsed'] : 'block';
  $collapsed = ($blockstyle == 'none') ? 'collapsed' : '';
  
  // WebKit browsers have an issue with jQueryUI draggables vs. CSS fixed positioning.
  // Sorry to the Safari and Chrome users, but we have to strip the fixed positioning out for this reason.
  // If user agent includes WebKit, degrade to absolute positioning, otherwise they can enjoy fixed.
  $position = (stristr($_SERVER['HTTP_USER_AGENT'], 'WebKit') === FALSE) ? 'fixed' : 'absolute';
  
  // Format our style strings to return for inclusion in the divs.
  $fmmstyle = sprintf(' style="top: %spx; left: %spx; position: %s;" class="%s"', 
    $top, $left, $position, $collapsed);
  $innerstyle = sprintf(' style="display: %s"', $blockstyle);
  $output = array($fmmstyle, $innerstyle);

  return $output;
}

/**
 * Generate a list of available skins.
 *
 * Read the file system to populate a list of skins that currently exist for the menu.
 *
 * @return
 *   Array of available skin folder names
 */
function floating_manager_menu_skin_list() {

  // This is the directory in which to store skins for this module.
  $dir = drupal_get_path('module', 'floating_manager_menu') . '/skins/';

  // Open a known directory, and proceed to read its contents
  if (is_dir($dir)) {
    if ($dh = opendir($dir)) {
      while (($file = readdir($dh)) !== FALSE) {
        // Only return folders that aren't . or ..
        if (is_dir($dir . $file) && !preg_match('/([.]{1,2})/', $file)) {
          // For convenience sake on the FAPI, it's easiest just to keep key and value the same here. 
          $skins[$file] = $file;
        }
      }
      closedir($dh);
    }
  }
  return $skins;
}

/**
 * Determine whether the menu should be shown, based on admin page exclusion.
 *
 * Determine if we are 1) on an admin page; and, 2) have opted to hide the menu on admin pages.
 *
 * @return
 *   TRUE if we are on an admin page and want to hide the menu, FALSE otherwise
 */
function floating_manager_menu_admin_hide() {
  // Is it an admin page?     Have we chosen to hide the menu on admin pages?
  if ((arg(0) == 'admin') && (variable_get('floating_manager_menu_adminexclude', 0))) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}