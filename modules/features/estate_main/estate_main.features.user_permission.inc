<?php
/**
 * @file
 * estate_main.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function estate_main_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access bean overview'.
  $permissions['access bean overview'] = array(
    'name' => 'access bean overview',
    'roles' => array(
      'Super admin' => 'Super admin',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'administer bean settings'.
  $permissions['administer bean settings'] = array(
    'name' => 'administer bean settings',
    'roles' => array(
      'Super admin' => 'Super admin',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'administer bean types'.
  $permissions['administer bean types'] = array(
    'name' => 'administer bean types',
    'roles' => array(
      'Super admin' => 'Super admin',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'administer beans'.
  $permissions['administer beans'] = array(
    'name' => 'administer beans',
    'roles' => array(
      'Super admin' => 'Super admin',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'administer blocks'.
  $permissions['administer blocks'] = array(
    'name' => 'administer blocks',
    'roles' => array(
      'Super admin' => 'Super admin',
    ),
    'module' => 'block',
  );

  // Exported permission: 'create any content bean'.
  $permissions['create any content bean'] = array(
    'name' => 'create any content bean',
    'roles' => array(),
    'module' => 'bean',
  );

  // Exported permission: 'delete any content bean'.
  $permissions['delete any content bean'] = array(
    'name' => 'delete any content bean',
    'roles' => array(),
    'module' => 'bean',
  );

  // Exported permission: 'edit any content bean'.
  $permissions['edit any content bean'] = array(
    'name' => 'edit any content bean',
    'roles' => array(),
    'module' => 'bean',
  );

  // Exported permission: 'edit bean view mode'.
  $permissions['edit bean view mode'] = array(
    'name' => 'edit bean view mode',
    'roles' => array(
      'Super admin' => 'Super admin',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'view any content bean'.
  $permissions['view any content bean'] = array(
    'name' => 'view any content bean',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'view bean page'.
  $permissions['view bean page'] = array(
    'name' => 'view bean page',
    'roles' => array(
      'Super admin' => 'Super admin',
    ),
    'module' => 'bean',
  );

  // Exported permission: 'view bean revisions'.
  $permissions['view bean revisions'] = array(
    'name' => 'view bean revisions',
    'roles' => array(
      'Super admin' => 'Super admin',
    ),
    'module' => 'bean',
  );

  return $permissions;
}
