<?php
/**
 * @file
 * estate_main.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function estate_main_filter_default_formats() {
  $formats = array();

  // Exported format: Visual editor.
  $formats['visual_editor'] = array(
    'format' => 'visual_editor',
    'name' => 'Visual editor',
    'cache' => 0,
    'status' => 1,
    'weight' => -9,
    'filters' => array(
      'image_resize_filter' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(
          'link' => 0,
          'link_class' => '',
          'link_rel' => '',
          'image_locations' => array(
            'local' => 'local',
            'remote' => 0,
          ),
        ),
      ),
      'shortcode' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(
          'relevant_estate' => 1,
        ),
      ),
      'filter_tokens' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_htmlcorrector' => array(
        'weight' => 10,
        'status' => 1,
        'settings' => array(),
      ),
    ),
  );

  return $formats;
}
