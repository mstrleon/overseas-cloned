<?php
/**
 * @file
 * estate_main.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function estate_main_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_amenities|node|estate|default';
  $field_group->group_name = 'group_amenities';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'estate';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Amenities',
    'weight' => '2',
    'children' => array(
      0 => 'field_amenities',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Amenities',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'node-block group-amenities field-group-div col-sm-6',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_amenities|node|estate|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_documents|node|estate|default';
  $field_group->group_name = 'group_documents';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'estate';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Documents',
    'weight' => '7',
    'children' => array(
      0 => 'field_attachments',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Documents',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'node-block group-documents field-group-div',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_documents|node|estate|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_files_attached|node|estate|form';
  $field_group->group_name = 'group_files_attached';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'estate';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_files';
  $field_group->data = array(
    'label' => 'Attached files',
    'weight' => '8',
    'children' => array(
      0 => 'field_attachments',
    ),
    'format_type' => 'bootstrap_fieldgroup_nav_item',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-files-attached field-group-htab',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_files_attached|node|estate|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_files_group|node|estate|form';
  $field_group->group_name = 'group_files_group';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'estate';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Files',
    'weight' => '3',
    'children' => array(
      0 => 'group_files',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Files',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => 'group-files-group field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_files_group|node|estate|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_files_photos|node|estate|form';
  $field_group->group_name = 'group_files_photos';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'estate';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_files';
  $field_group->data = array(
    'label' => 'Фото',
    'weight' => '5',
    'children' => array(
      0 => 'field_photos',
    ),
    'format_type' => 'bootstrap_fieldgroup_nav_item',
    'format_settings' => array(
      'label' => 'Фото',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => 'group-files-photos field-group-tab',
      ),
    ),
  );
  $field_groups['group_files_photos|node|estate|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_files|node|estate|form';
  $field_group->group_name = 'group_files';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'estate';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_files_group';
  $field_group->data = array(
    'label' => 'Files tab',
    'weight' => '4',
    'children' => array(
      0 => 'group_files_attached',
      1 => 'group_files_photos',
    ),
    'format_type' => 'bootstrap_fieldgroup_nav',
    'format_settings' => array(
      'label' => 'Files tab',
      'instance_settings' => array(
        'bootstrap_nav_type' => 'tabs',
        'bootstrap_stacked' => '0',
        'bootstrap_orientation' => '0',
        'required_fields' => 0,
        'classes' => 'group-files field-group-tabs',
      ),
    ),
  );
  $field_groups['group_files|node|estate|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_key_values|node|estate|default';
  $field_group->group_name = 'group_key_values';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'estate';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Key details',
    'weight' => '1',
    'children' => array(
      0 => 'field_mortgage',
      1 => 'field_sale_status',
      2 => 'field_price',
      3 => 'field_square_total',
      4 => 'field_square_live',
      5 => 'field_floors_total',
      6 => 'field_floor',
      7 => 'field_rooms_total',
      8 => 'field_estate_type',
      9 => 'field_sku',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Key details',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'node-block group-key-values field-group-fieldset col-sm-6',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_key_values|node|estate|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_links|node|blog|form';
  $field_group->group_name = 'group_links';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'blog';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Linked objects',
    'weight' => '2',
    'children' => array(
      0 => 'field_district',
      1 => 'field_object_id',
      2 => 'field_estate_tags',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Linked objects',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_links|node|blog|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_location|node|estate|default';
  $field_group->group_name = 'group_location';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'estate';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Location',
    'weight' => '4',
    'children' => array(
      0 => 'field_location',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Location',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'node-block group-location field-group-div col-sm-12',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_location|node|estate|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_overview|node|estate|default';
  $field_group->group_name = 'group_overview';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'estate';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Overview',
    'weight' => '3',
    'children' => array(
      0 => 'body',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Overview',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'node-block group-overview field-group-div col-sm-12',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_overview|node|estate|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_photos|node|estate|default';
  $field_group->group_name = 'group_photos';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'estate';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Photos',
    'weight' => '0',
    'children' => array(
      0 => 'field_photos',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Photos',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'node-block group-photos field-group-div col-sm-12',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h2',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_photos|node|estate|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_properties|node|estate|teaser';
  $field_group->group_name = 'group_properties';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'estate';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Properties',
    'weight' => '6',
    'children' => array(
      0 => 'field_sale_status',
      1 => 'field_square_total',
      2 => 'field_rooms_total',
      3 => 'field_estate_type',
      4 => 'added_date',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Properties',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-properties field-group-div clearfix',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_properties|node|estate|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_subheader|node|estate|teaser';
  $field_group->group_name = 'group_subheader';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'estate';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Subheader',
    'weight' => '2',
    'children' => array(
      0 => 'field_price',
      1 => 'field_location',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Subheader',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-subheader field-group-div clearfix',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_subheader|node|estate|teaser'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Amenities');
  t('Attached files');
  t('Documents');
  t('Files');
  t('Files tab');
  t('Key details');
  t('Linked objects');
  t('Location');
  t('Overview');
  t('Photos');
  t('Properties');
  t('Subheader');
  t('Фото');

  return $field_groups;
}
