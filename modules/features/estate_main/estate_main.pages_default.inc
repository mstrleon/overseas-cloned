<?php
/**
 * @file
 * estate_main.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function estate_main_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_edit__edit';
  $handler->task = 'node_edit';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 1;
  $handler->conf = array(
    'title' => 'Estate',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'edit',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'estate' => 'estate',
            ),
          ),
          'context' => 'argument_node_edit_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'bootstrap_twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => array(
        'column_type' => 'col-lg',
        'column_size' => '8',
        'offset_type' => 'col-lg-offset',
        'offset_size' => '0',
      ),
      'right' => array(
        'column_type' => 'col-lg',
        'column_size' => '4',
        'offset_type' => 'col-lg-offset',
        'offset_size' => '0',
      ),
      'bottom' => NULL,
      'sidebar' => NULL,
      'content_main' => NULL,
    ),
    'left' => array(
      'style' => 'bootstrap_region',
    ),
    'right' => array(
      'style' => 'bootstrap_region',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'b2e72232-f7b0-4d76-ada3-50ab07c8f074';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'node_edit__edit';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-caa46988-9cdc-4c7c-9253-67c8fc22ba02';
  $pane->panel = 'left';
  $pane->type = 'node_form_title';
  $pane->subtype = 'node_form_title';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'argument_node_edit_1',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'caa46988-9cdc-4c7c-9253-67c8fc22ba02';
  $display->content['new-caa46988-9cdc-4c7c-9253-67c8fc22ba02'] = $pane;
  $display->panels['left'][0] = 'new-caa46988-9cdc-4c7c-9253-67c8fc22ba02';
  $pane = new stdClass();
  $pane->pid = 'new-343b989f-b2b4-4a83-b701-13891e8c5341';
  $pane->panel = 'left';
  $pane->type = 'entity_form_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => '',
    'formatter' => '',
    'context' => 'argument_node_edit_1',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '343b989f-b2b4-4a83-b701-13891e8c5341';
  $display->content['new-343b989f-b2b4-4a83-b701-13891e8c5341'] = $pane;
  $display->panels['left'][1] = 'new-343b989f-b2b4-4a83-b701-13891e8c5341';
  $pane = new stdClass();
  $pane->pid = 'new-fd633aca-386e-4291-93d5-6d5294cb56e6';
  $pane->panel = 'right';
  $pane->type = 'entity_form_field';
  $pane->subtype = 'node:field_mortgage';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => '',
    'formatter' => '',
    'context' => 'argument_node_edit_1',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'fd633aca-386e-4291-93d5-6d5294cb56e6';
  $display->content['new-fd633aca-386e-4291-93d5-6d5294cb56e6'] = $pane;
  $display->panels['right'][0] = 'new-fd633aca-386e-4291-93d5-6d5294cb56e6';
  $display->hide_title = PANELS_TITLE_PANE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_edit__edit'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'term_view__panel_context_ba853a09-292a-4f88-bfda-7d43ba8497ae';
  $handler->task = 'term_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Region (Country / City)',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'term_vocabulary',
          'settings' => array(
            'machine_name' => array(
              'region' => 'region',
            ),
          ),
          'context' => 'argument_term_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => array(),
      'middle' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
    'left' => array(
      'style' => 'bootstrap_region',
    ),
  );
  $display->cache = array();
  $display->title = '%term:name';
  $display->uuid = 'c55b1b35-d525-414a-94d2-3093322c3e1f';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'term_view__panel_context_ba853a09-292a-4f88-bfda-7d43ba8497ae';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-d6e83a0d-8613-4982-a18c-f7ba4f87cfc4';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'views-f1f60dc2ad49ad13262854a91cccb7e1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'inherit_path' => 1,
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd6e83a0d-8613-4982-a18c-f7ba4f87cfc4';
  $display->content['new-d6e83a0d-8613-4982-a18c-f7ba4f87cfc4'] = $pane;
  $display->panels['middle'][0] = 'new-d6e83a0d-8613-4982-a18c-f7ba4f87cfc4';
  $pane = new stdClass();
  $pane->pid = 'new-29b1605e-ac82-49d0-a234-11046a9e80df';
  $pane->panel = 'middle';
  $pane->type = 'entity_view';
  $pane->subtype = 'taxonomy_term';
  $pane->shown = FALSE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'full',
    'context' => 'argument_term_1',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '29b1605e-ac82-49d0-a234-11046a9e80df';
  $display->content['new-29b1605e-ac82-49d0-a234-11046a9e80df'] = $pane;
  $display->panels['middle'][1] = 'new-29b1605e-ac82-49d0-a234-11046a9e80df';
  $pane = new stdClass();
  $pane->pid = 'new-9f809e78-dd52-4e7e-8150-f7224156aad2';
  $pane->panel = 'middle';
  $pane->type = 'views_panes';
  $pane->subtype = 'estate_listing-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'arguments' => array(
      'term_node_tid_depth' => '%term:tid',
    ),
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
    'items_per_page' => '15',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '9f809e78-dd52-4e7e-8150-f7224156aad2';
  $display->content['new-9f809e78-dd52-4e7e-8150-f7224156aad2'] = $pane;
  $display->panels['middle'][2] = 'new-9f809e78-dd52-4e7e-8150-f7224156aad2';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['term_view__panel_context_ba853a09-292a-4f88-bfda-7d43ba8497ae'] = $handler;

  return $export;
}

/**
 * Implements hook_default_page_manager_pages().
 */
function estate_main_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'front';
  $page->task = 'page';
  $page->admin_title = 'Front';
  $page->admin_description = 'Front page';
  $page->path = 'frontpage';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_front__panel_context_a0a9b02f-f55b-4d69-86c2-a63266acbd6c';
  $handler->task = 'page';
  $handler->subtask = 'front';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Панель',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => array(
        'content' => array(
          'element' => 'div',
          'attributes' => array(
            'id' => '',
            'class' => '',
          ),
        ),
        'theme' => 0,
      ),
      'middle' => array(
        'content' => array(
          'element' => 'div',
          'attributes' => array(
            'id' => 'front-panel',
            'class' => '',
          ),
        ),
        'theme' => 0,
      ),
    ),
    'style' => 'wrapper_element',
    'middle' => array(
      'style' => 'wrapper_element',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'd647e200-f967-4a3f-b7be-3397b38a889a';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_front__panel_context_a0a9b02f-f55b-4d69-86c2-a63266acbd6c';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-f2ee3b90-ca76-4e85-8f3c-1468a2ed097a';
  $pane->panel = 'middle';
  $pane->type = 'panels_mini';
  $pane->subtype = 'front_about_search';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'f2ee3b90-ca76-4e85-8f3c-1468a2ed097a';
  $display->content['new-f2ee3b90-ca76-4e85-8f3c-1468a2ed097a'] = $pane;
  $display->panels['middle'][0] = 'new-f2ee3b90-ca76-4e85-8f3c-1468a2ed097a';
  $pane = new stdClass();
  $pane->pid = 'new-5441ccae-7282-47fe-bfea-4ed76ef832a3';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'bean-o-kompanii-blok-na-glavnoy';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '5441ccae-7282-47fe-bfea-4ed76ef832a3';
  $display->content['new-5441ccae-7282-47fe-bfea-4ed76ef832a3'] = $pane;
  $display->panels['middle'][1] = 'new-5441ccae-7282-47fe-bfea-4ed76ef832a3';
  $pane = new stdClass();
  $pane->pid = 'new-38076e64-5fc4-4174-929f-a1d3abebf8ed';
  $pane->panel = 'middle';
  $pane->type = 'views_panes';
  $pane->subtype = 'estate_listing-panel_pane_2';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'use_pager' => 0,
    'pager_id' => '0',
    'items_per_page' => '6',
    'override_title' => 1,
    'override_title_text' => 'Новые предложения',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '38076e64-5fc4-4174-929f-a1d3abebf8ed';
  $display->content['new-38076e64-5fc4-4174-929f-a1d3abebf8ed'] = $pane;
  $display->panels['middle'][2] = 'new-38076e64-5fc4-4174-929f-a1d3abebf8ed';
  $pane = new stdClass();
  $pane->pid = 'new-f9b42730-b859-427d-92de-c3ccaff5f317';
  $pane->panel = 'middle';
  $pane->type = 'views_panes';
  $pane->subtype = 'estate_listing-panel_pane_3';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'use_pager' => 0,
    'pager_id' => '0',
    'items_per_page' => '6',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array();
  $pane->uuid = 'f9b42730-b859-427d-92de-c3ccaff5f317';
  $display->content['new-f9b42730-b859-427d-92de-c3ccaff5f317'] = $pane;
  $display->panels['middle'][3] = 'new-f9b42730-b859-427d-92de-c3ccaff5f317';
  $pane = new stdClass();
  $pane->pid = 'new-cae4c65c-00c3-44ca-b17e-213a3647d0f1';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'bean-banner-na-glavnoy';
  $pane->shown = FALSE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 4;
  $pane->locks = array();
  $pane->uuid = 'cae4c65c-00c3-44ca-b17e-213a3647d0f1';
  $display->content['new-cae4c65c-00c3-44ca-b17e-213a3647d0f1'] = $pane;
  $display->panels['middle'][4] = 'new-cae4c65c-00c3-44ca-b17e-213a3647d0f1';
  $pane = new stdClass();
  $pane->pid = 'new-068e3554-67af-4233-8e98-8ec4d282da27';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'blog_listing';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 0,
    'nodes_per_page' => '6',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'block_2',
    'override_title' => 1,
    'override_title_text' => 'Статьи',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => array(),
    'style' => 'stylizer',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 5;
  $pane->locks = array();
  $pane->uuid = '068e3554-67af-4233-8e98-8ec4d282da27';
  $display->content['new-068e3554-67af-4233-8e98-8ec4d282da27'] = $pane;
  $display->panels['middle'][5] = 'new-068e3554-67af-4233-8e98-8ec4d282da27';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['front'] = $page;

  return $pages;

}
