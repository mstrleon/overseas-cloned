<?php
/**
 * @file
 * estate_main.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function estate_main_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "bean_admin_ui" && $api == "bean") {
    return array("version" => "5");
  }
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  if ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function estate_main_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function estate_main_image_default_styles() {
  $styles = array();

  // Exported image style: full_gallery_thumbs.
  $styles['full_gallery_thumbs'] = array(
    'label' => 'Full gallery thumbs',
    'effects' => array(
      14 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1200,
          'height' => 860,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: full_width.
  $styles['full_width'] = array(
    'label' => 'Full width (1200 +upscale)',
    'effects' => array(
      9 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 1200,
          'height' => '',
          'upscale' => 1,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function estate_main_node_info() {
  $items = array(
    'blog' => array(
      'name' => t('Статья'),
      'base' => 'node_content',
      'description' => t('Blog post.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'estate' => array(
      'name' => t('Недвижимость'),
      'base' => 'node_content',
      'description' => t('Real estate object.'),
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'help' => '',
    ),
    'information' => array(
      'name' => t('Информация'),
      'base' => 'node_content',
      'description' => t('Информационные страницы для стран/городов.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
