<?php
/**
 * @file
 * estate_main.features.translations_ru.inc
 */

/**
 * Implements hook_translations_ru_defaults().
 */
function estate_main_translations_ru_defaults() {
  $translations = array();
  $translatables = array();
  $translations['ru:default']['67b0b8a8f8a2e86b7a76cfe0cbd03197'] = array(
    'source' => '@count[2] objects',
    'context' => '',
    'location' => '/buy?shs_term_node_tid_depth=65&field_price_value%5Bmin%5D=&field_price_value%5Bmax%5D=&sort_bef_combine=created+DESC&field_estate_type_tid=All&field_square_total_value%5Bmin%5D=&field_square_total_value%5Bmax%5D=',
    'translation' => '@count[2] предложений',
    'plid' => 0,
    'plural' => 0,
  );
  $translatables[] = t('@count[2] objects', array(), array('context' => ''));
  $translations['ru:default']['cd559071672ede7a67947a7ddbca35c7'] = array(
    'source' => '@count object',
    'context' => '',
    'location' => '/buy?shs_term_node_tid_depth=65&field_price_value%5Bmin%5D=&field_price_value%5Bmax%5D=&sort_bef_combine=created+DESC&field_estate_type_tid=All&field_square_total_value%5Bmin%5D=&field_square_total_value%5Bmax%5D=',
    'translation' => '@count предложение',
    'plid' => 0,
    'plural' => 0,
  );
  $translatables[] = t('@count object', array(), array('context' => ''));
  $translations['ru:default']['df498286bdf06c35bbc5bd6c17d065f2'] = array(
    'source' => '@count objects',
    'context' => '',
    'location' => '/buy?shs_term_node_tid_depth=65&field_price_value%5Bmin%5D=&field_price_value%5Bmax%5D=&sort_bef_combine=created+DESC&field_estate_type_tid=All&field_square_total_value%5Bmin%5D=&field_square_total_value%5Bmax%5D=',
    'translation' => '@count предложения',
    'plid' => 0,
    'plural' => 0,
  );
  $translatables[] = t('@count objects', array(), array('context' => ''));
  $translations['ru:default']['e34a3c57feed96dc0e45a98d550d69d8'] = array(
    'source' => '<Any>',
    'context' => '',
    'location' => '',
    'translation' => '- Любой -',
    'plid' => 0,
    'plural' => 0,
  );
  $translatables[] = t('<Any>', array(), array('context' => ''));
  $translations['ru:views']['143e5378f626fff9bda854a3af44e0b1'] = array(
    'source' => 'Sorting',
    'context' => 'estate_listing:block_1:exposed_form:exposed_sorts_label',
    'location' => 'views:estate_listing:block_1:exposed_form:exposed_sorts_label',
    'translation' => 'Сортировка',
    'plid' => 0,
    'plural' => 0,
  );
  $translatables[] = t('Sorting', array(), array('context' => 'estate_listing:block_1:exposed_form:exposed_sorts_label'));
  $translations['ru:views']['50ff65f1e35bea6d72a6a3ece21417a0'] = array(
    'source' => 'Sorting',
    'context' => 'estate_listing:block_2:exposed_form:exposed_sorts_label',
    'location' => 'views:estate_listing:block_2:exposed_form:exposed_sorts_label',
    'translation' => 'Сортировка',
    'plid' => 0,
    'plural' => 0,
  );
  $translatables[] = t('Sorting', array(), array('context' => 'estate_listing:block_2:exposed_form:exposed_sorts_label'));
  $translations['ru:views']['85149b8ab9c59ebed0661ed672e91092'] = array(
    'source' => 'Sort by',
    'context' => 'estate_listing:page:exposed_form:exposed_sorts_label',
    'location' => 'views:estate_listing:page:exposed_form:exposed_sorts_label',
    'translation' => 'Сортировка',
    'plid' => 0,
    'plural' => 0,
  );
  $translatables[] = t('Sort by', array(), array('context' => 'estate_listing:page:exposed_form:exposed_sorts_label'));
  $translations['ru:views']['abc5fbeb3c850071ad618c8738f3ce02'] = array(
    'source' => 'Post date Asc|Date: oldest first
Post date Desc|Date: newest first
Price Asc|Price: low to high
Price Desc|Price: high to low',
    'context' => 'estate_listing:default:bef:sort:advanced:combine_rewrite',
    'location' => 'views:estate_listing:default:bef:sort:advanced:combine_rewrite',
    'translation' => 'Post date Asc|Date: oldest first
Post date Desc|Date: newest first
Price Asc|Price: low to high
Price Desc|Price: high to low',
    'plid' => 0,
    'plural' => 0,
  );
  $translatables[] = t('Post date Asc|Date: oldest first
Post date Desc|Date: newest first
Price Asc|Price: low to high
Price Desc|Price: high to low', array(), array('context' => 'estate_listing:default:bef:sort:advanced:combine_rewrite'));
  return $translations;
}
