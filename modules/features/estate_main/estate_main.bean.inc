<?php
/**
 * @file
 * estate_main.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function estate_main_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'content';
  $bean_type->label = 'Content';
  $bean_type->options = '';
  $bean_type->description = 'Content blocks for panels and so on.';
  $export['content'] = $bean_type;

  return $export;
}
