<?php
/**
 * @file
 * estate_main.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function estate_main_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'article_node';
  $context->description = 'Блоки на странце СТАТЬИ';
  $context->tag = 'pages';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'blog' => 'blog',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-blog_listing-block_1' => array(
          'module' => 'views',
          'delta' => 'blog_listing-block_1',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('pages');
  t('Блоки на странце СТАТЬИ');
  $export['article_node'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'buy_page';
  $context->description = 'Buy page blocks';
  $context->tag = 'pages';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'estate_listing:page_1' => 'estate_listing:page_1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views--exp-estate_listing-page_1' => array(
          'module' => 'views',
          'delta' => '-exp-estate_listing-page_1',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views-cards_country_city_types-block' => array(
          'module' => 'views',
          'delta' => 'cards_country_city_types-block',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'views-blog_listing-block_1' => array(
          'module' => 'views',
          'delta' => 'blog_listing-block_1',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Buy page blocks');
  t('pages');
  $export['buy_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'no_first_sidebar';
  $context->description = 'No first (left) sidebar based on path';
  $context->tag = 'global';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'partners' => 'partners',
        'contacts' => 'contacts',
        'about' => 'about',
      ),
    ),
  );
  $context->reactions = array(
    'region' => array(
      'estate' => array(
        'disable' => array(
          'sidebar_first' => 'sidebar_first',
          'navigation' => 0,
          'header' => 0,
          'header_extra' => 0,
          'highlighted' => 0,
          'help' => 0,
          'content' => 0,
          'sidebar_second' => 0,
          'footer' => 0,
          'page_top' => 0,
          'page_bottom' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('No first (left) sidebar based on path');
  t('global');
  $export['no_first_sidebar'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'no_sidebars';
  $context->description = 'Disable sidebars (based on path - add/edit if needed)';
  $context->tag = 'global';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
        'node/*/edit' => 'node/*/edit',
        'node/*/edit/*' => 'node/*/edit/*',
        'node/*/webform*' => 'node/*/webform*',
        'node/add/*' => 'node/add/*',
        'articles' => 'articles',
      ),
    ),
  );
  $context->reactions = array(
    'region' => array(
      'estate' => array(
        'disable' => array(
          'sidebar_first' => 'sidebar_first',
          'sidebar_second' => 'sidebar_second',
          'navigation' => 0,
          'header' => 0,
          'header_extra' => 0,
          'highlighted' => 0,
          'help' => 0,
          'content' => 0,
          'footer' => 0,
          'page_top' => 0,
          'page_bottom' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Disable sidebars (based on path - add/edit if needed)');
  t('global');
  $export['no_sidebars'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'taxonomy_page_blocks';
  $context->description = 'Blocks at region listing / full-nodes (estate, newbuild)';
  $context->tag = 'pages';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'estate' => 'estate',
        'newbuild' => 'newbuild',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        'taxonomy/term/*' => 'taxonomy/term/*',
      ),
    ),
    'taxonomy_term' => array(
      'values' => array(
        'region' => 'region',
      ),
      'options' => array(
        'term_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'estate_display-estate_country_info' => array(
          'module' => 'estate_display',
          'delta' => 'estate_country_info',
          'region' => 'sidebar_first',
          'weight' => '-28',
        ),
        'views-cards_country_city_types-block' => array(
          'module' => 'views',
          'delta' => 'cards_country_city_types-block',
          'region' => 'sidebar_first',
          'weight' => '-27',
        ),
        'views-blog_listing-block_1' => array(
          'module' => 'views',
          'delta' => 'blog_listing-block_1',
          'region' => 'sidebar_first',
          'weight' => NULL,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks at region listing / full-nodes (estate, newbuild)');
  t('pages');
  $export['taxonomy_page_blocks'] = $context;

  return $export;
}
