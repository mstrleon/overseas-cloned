<?php
/**
 * @file
 * estate_main.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function estate_main_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'estate_amenities';
  $mini->category = '';
  $mini->admin_title = 'Estate amenities';
  $mini->admin_description = 'Amenities test pane.';
  $mini->requiredcontexts = array(
    0 => array(
      'identifier' => 'Node',
      'keyword' => 'node',
      'name' => 'entity:node',
      'entity_id' => '',
      'optional' => 0,
      'id' => 1,
    ),
  );
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'bootstrap_twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
      'top' => NULL,
      'right' => array(
        'column_type' => 'col-sm',
        'column_size' => '4',
        'offset_type' => 'col-lg-offset',
        'offset_size' => '0',
      ),
      'bottom' => array(
        'column_type' => 'col-sm',
        'column_size' => '12',
        'offset_type' => 'col-lg-offset',
        'offset_size' => '0',
      ),
      'left' => array(
        'column_type' => 'col-sm',
        'column_size' => '8',
        'offset_type' => 'col-lg-offset',
        'offset_size' => '0',
      ),
    ),
    'left' => array(
      'style' => 'bootstrap_region',
    ),
    'right' => array(
      'style' => 'bootstrap_region',
    ),
    'bottom' => array(
      'style' => 'bootstrap_region',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '1a28164e-9e5c-46cd-99db-e0edf85176ea';
  $display->storage_type = 'panels_mini';
  $display->storage_id = 'estate_amenities';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-f5ba8e4e-87c7-4cb2-9e6f-5234e07e3920';
  $pane->panel = 'bottom';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_amenities';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'taxonomy_term_reference_plain',
    'delta_limit' => '0',
    'delta_offset' => '0',
    'delta_reversed' => 0,
    'formatter_settings' => array(),
    'context' => 'requiredcontext_entity:node_1',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'f5ba8e4e-87c7-4cb2-9e6f-5234e07e3920';
  $display->content['new-f5ba8e4e-87c7-4cb2-9e6f-5234e07e3920'] = $pane;
  $display->panels['bottom'][0] = 'new-f5ba8e4e-87c7-4cb2-9e6f-5234e07e3920';
  $pane = new stdClass();
  $pane->pid = 'new-dc7a653e-94ec-4f95-a12e-cb735f7dbedd';
  $pane->panel = 'left';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_rooms_total';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'inline',
    'formatter' => 'number_integer',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'thousand_separator' => '',
      'prefix_suffix' => 0,
    ),
    'context' => 'requiredcontext_entity:node_1',
    'override_title' => 1,
    'override_title_text' => 'Комнат',
    'override_title_heading' => 'h6',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'dc7a653e-94ec-4f95-a12e-cb735f7dbedd';
  $display->content['new-dc7a653e-94ec-4f95-a12e-cb735f7dbedd'] = $pane;
  $display->panels['left'][0] = 'new-dc7a653e-94ec-4f95-a12e-cb735f7dbedd';
  $pane = new stdClass();
  $pane->pid = 'new-a5e92657-4456-416e-9fdd-d8d4fc79ba7d';
  $pane->panel = 'left';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_square_total';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'inline',
    'formatter' => 'number_decimal',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'thousand_separator' => '',
      'decimal_separator' => '.',
      'scale' => '0',
      'prefix_suffix' => 1,
    ),
    'context' => 'requiredcontext_entity:node_1',
    'override_title' => 1,
    'override_title_text' => 'Площадь',
    'override_title_heading' => 'h6',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'a5e92657-4456-416e-9fdd-d8d4fc79ba7d';
  $display->content['new-a5e92657-4456-416e-9fdd-d8d4fc79ba7d'] = $pane;
  $display->panels['left'][1] = 'new-a5e92657-4456-416e-9fdd-d8d4fc79ba7d';
  $pane = new stdClass();
  $pane->pid = 'new-a4a5489f-a38a-42a1-8354-c4868dd0ea3b';
  $pane->panel = 'left';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_floor';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'inline',
    'formatter' => 'number_integer',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'thousand_separator' => '',
      'prefix_suffix' => 1,
    ),
    'context' => 'requiredcontext_entity:node_1',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'a4a5489f-a38a-42a1-8354-c4868dd0ea3b';
  $display->content['new-a4a5489f-a38a-42a1-8354-c4868dd0ea3b'] = $pane;
  $display->panels['left'][2] = 'new-a4a5489f-a38a-42a1-8354-c4868dd0ea3b';
  $pane = new stdClass();
  $pane->pid = 'new-ec5dbb1d-55ad-4e1d-866a-686c358e564e';
  $pane->panel = 'right';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_price';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'number_decimal',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'thousand_separator' => ' ',
      'decimal_separator' => '.',
      'scale' => '0',
      'prefix_suffix' => 1,
    ),
    'context' => 'requiredcontext_entity:node_1',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'naked',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'ec5dbb1d-55ad-4e1d-866a-686c358e564e';
  $display->content['new-ec5dbb1d-55ad-4e1d-866a-686c358e564e'] = $pane;
  $display->panels['right'][0] = 'new-ec5dbb1d-55ad-4e1d-866a-686c358e564e';
  $pane = new stdClass();
  $pane->pid = 'new-e6222765-46fe-4b04-a142-2c864b9fab26';
  $pane->panel = 'right';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'admin_title' => 'Price for sq.meter',
    'title' => '',
    'title_heading' => 'h2',
    'body' => '<div class="subtitle">[estate_sq_price:%node:field_price|%node:field_square_total] &euro;/м<sup>2</sup></div>',
    'format' => 'visual_editor',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'e6222765-46fe-4b04-a142-2c864b9fab26';
  $display->content['new-e6222765-46fe-4b04-a142-2c864b9fab26'] = $pane;
  $display->panels['right'][1] = 'new-e6222765-46fe-4b04-a142-2c864b9fab26';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-ec5dbb1d-55ad-4e1d-866a-686c358e564e';
  $mini->display = $display;
  $export['estate_amenities'] = $mini;

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'front_about_search';
  $mini->category = '';
  $mini->admin_title = 'Поиск и о компании (front)';
  $mini->admin_description = 'Блок поиска для главной.';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'bootstrap_twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'left' => array(
        'column_type' => 'col-sm',
        'column_size' => '4',
        'offset_type' => 'col-lg-offset',
        'offset_size' => '0',
      ),
      'right' => array(
        'column_type' => 'col-sm',
        'column_size' => '8',
        'offset_type' => 'col-lg-offset',
        'offset_size' => '0',
      ),
      'top' => NULL,
      'bottom' => NULL,
      'default' => NULL,
    ),
    'left' => array(
      'style' => 'bootstrap_region',
    ),
    'right' => array(
      'style' => 'bootstrap_region',
    ),
    'top' => array(
      'style' => 'naked',
    ),
    'bottom' => array(
      'style' => 'naked',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '6239f965-96c0-46d9-822d-15c9f8a71f13';
  $display->storage_type = 'panels_mini';
  $display->storage_id = 'front_about_search';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-8aa228a7-d051-45b7-bf64-78e9c33b6150';
  $pane->panel = 'left';
  $pane->type = 'node';
  $pane->subtype = 'node';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'nid' => '646',
    'links' => 0,
    'leave_node_title' => 0,
    'identifier' => '',
    'build_mode' => 'teaser',
    'link_node_title' => 0,
    'override_title' => 1,
    'override_title_text' => '<none>',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '8aa228a7-d051-45b7-bf64-78e9c33b6150';
  $display->content['new-8aa228a7-d051-45b7-bf64-78e9c33b6150'] = $pane;
  $display->panels['left'][0] = 'new-8aa228a7-d051-45b7-bf64-78e9c33b6150';
  $pane = new stdClass();
  $pane->pid = 'new-8323f793-7201-42ef-ab34-434eaaf8c40e';
  $pane->panel = 'right';
  $pane->type = 'block';
  $pane->subtype = 'views--exp-estate_listing-page_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'inherit_path' => 1,
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '8323f793-7201-42ef-ab34-434eaaf8c40e';
  $display->content['new-8323f793-7201-42ef-ab34-434eaaf8c40e'] = $pane;
  $display->panels['right'][0] = 'new-8323f793-7201-42ef-ab34-434eaaf8c40e';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-8aa228a7-d051-45b7-bf64-78e9c33b6150';
  $mini->display = $display;
  $export['front_about_search'] = $mini;

  return $export;
}
