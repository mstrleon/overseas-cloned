<?php
/**
 * @file
 * estate_main.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function estate_main_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|estate|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'estate';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'bootstrap_9_3';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'title_field',
        1 => 'field_place',
        2 => 'body',
        3 => 'group_files_group',
        4 => 'field_location',
        5 => 'group_files',
        6 => 'field_attachments',
        8 => 'group_files_photos',
        11 => 'group_files_attached',
        28 => 'field_photos',
      ),
      'right' => array(
        7 => 'field_sku',
        9 => 'field_price',
        10 => 'field_estate_type',
        12 => 'field_sale_status',
        13 => 'field_build_year',
        14 => 'field_rooms_total',
        15 => 'field_bedrooms_num',
        16 => 'field_bathrooms_num',
        17 => 'field_square_total',
        18 => 'field_square_live',
        19 => 'field_square_land',
        20 => 'field_floor',
        21 => 'field_floors_total',
        22 => 'field_view',
        23 => 'field_estate_tags',
        24 => 'field_amenities',
      ),
      'hidden' => array(
        25 => 'field_mortgage',
        26 => 'field_external_src',
        27 => 'language',
        29 => 'path',
        30 => 'field_show_in_header',
        31 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'title_field' => 'left',
      'field_place' => 'left',
      'body' => 'left',
      'group_files_group' => 'left',
      'field_location' => 'left',
      'group_files' => 'left',
      'field_attachments' => 'left',
      'field_sku' => 'right',
      'group_files_photos' => 'left',
      'field_price' => 'right',
      'field_estate_type' => 'right',
      'group_files_attached' => 'left',
      'field_sale_status' => 'right',
      'field_build_year' => 'right',
      'field_rooms_total' => 'right',
      'field_bedrooms_num' => 'right',
      'field_bathrooms_num' => 'right',
      'field_square_total' => 'right',
      'field_square_live' => 'right',
      'field_square_land' => 'right',
      'field_floor' => 'right',
      'field_floors_total' => 'right',
      'field_view' => 'right',
      'field_estate_tags' => 'right',
      'field_amenities' => 'right',
      'field_mortgage' => 'hidden',
      'field_external_src' => 'hidden',
      'language' => 'hidden',
      'field_photos' => 'left',
      'path' => 'hidden',
      'field_show_in_header' => 'hidden',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|estate|form'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function estate_main_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'block_like';
  $ds_view_mode->label = 'Block-like';
  $ds_view_mode->entities = array(
    'node' => 'node',
    'taxonomy_term' => 'taxonomy_term',
    'user' => 'user',
  );
  $export['block_like'] = $ds_view_mode;

  return $export;
}
