<?php
/**
 * @file
 * estate_new_building.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function estate_new_building_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|newbuild|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'newbuild';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'bootstrap_9_3';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'title',
        1 => 'body',
        2 => 'field_photos',
        3 => 'path',
        4 => 'field_location',
      ),
      'right' => array(
        5 => 'field_price',
        6 => 'field_place',
      ),
      'hidden' => array(
        7 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'title' => 'left',
      'body' => 'left',
      'field_photos' => 'left',
      'path' => 'left',
      'field_location' => 'left',
      'field_price' => 'right',
      'field_place' => 'right',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|newbuild|form'] = $ds_layout;

  return $export;
}
