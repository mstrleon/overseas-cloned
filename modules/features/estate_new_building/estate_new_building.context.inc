<?php
/**
 * @file
 * estate_new_building.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function estate_new_building_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'newbuild_page';
  $context->description = 'New build listing page';
  $context->tag = 'pages';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'estate_listing:page_2' => 'estate_listing:page_2',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-cards_country_city_types-block' => array(
          'module' => 'views',
          'delta' => 'cards_country_city_types-block',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'views-blog_listing-block_1' => array(
          'module' => 'views',
          'delta' => 'blog_listing-block_1',
          'region' => 'sidebar_second',
          'weight' => '3',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('New build listing page');
  t('pages');
  $export['newbuild_page'] = $context;

  return $export;
}
