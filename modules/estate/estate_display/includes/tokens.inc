<?php
function estate_display_token__token_name(&$replacements, $tokens, $data, $options) {
  foreach ($tokens as $id => $original) {
    $val = explode(':', $id);
    $replacements[$original] = format_string('<h2 class="feature-header @class">@title</h2>', [ '@class' => $val[0], '@title' => $val[1] ]);
  }
}

function estate_display_token__estate_info_front(&$replacements, $tokens, $data, $options) {
  foreach ($tokens as $id => $original) {
    $node = node_load($id);
    if (!$node) continue;
    
    $vals['price'] = estate_display_get_field_value_custom('field_price', $node, 0, 'teaser', ['thousand_separator' => ' '])['#markup'];
    $vals['square'] = estate_display_get_field_value_custom('field_square_total', $node, 0, 'teaser', ['scale' => 0])['#markup'];
    
    if (!empty($node->field_rooms_total['und'][0]['value']))
      $vals['rooms'] = 'Комнат: ' . $node->field_rooms_total['und'][0]['value'];
  
    $string = '';
    foreach ($vals as $k => $val) {
      $string .= "<div class='fld fld-$k'>$val</div>";
    }
    
    $link = url('node/' . $node->nid);
    $replacements[$original] = "<div class='prop-row'><a href='$link'>$string</a></div>";
  }
}

function estate_display_token__newbuild_info_blocklike(&$replacements, $tokens, $data, $options) {
  foreach ($tokens as $id => $original) {
    $node = node_load($id);
    if (!$node) continue;
    
    $vals['place'] = estate_display_get_field_value_custom('field_place', $node, 0, 'default');
    $vals['place'] = drupal_render($vals['place']);
    
    $vals['price'] = estate_display_get_field_value_custom('field_price', $node, 0, 'default', ['thousand_separator' => ' ', 'scale' => 0]);
    $vals['price'] = $vals['price']['#markup'] ? 'от ' . $vals['price']['#markup'] : '';
    
    if (!empty($node->field_rooms_total['und'][0]['value']))
      $vals['rooms'] = 'Комнат: ' . $node->field_rooms_total['und'][0]['value'];
  
    $string = '';
    foreach ($vals as $k => $val) {
      $string .= "<div class='fld fld-$k'>$val</div>";
    }
    
    $replacements[$original] = $string;
  }
}