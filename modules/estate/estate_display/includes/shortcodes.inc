<?php
/**
 * Created by PhpStorm.
 * User: Руслан
 * Date: 11.01.2017
 * Time: 11:02
 */

/**
 * Process callback for Relevant Estate shortcode.
 *
 * @param $attrs
 * @param $text
 * @return
 */
function estate_display_shortcode_relevant_estate($attrs, $text) {
  $attrs = shortcode_attrs(array(
    'type' => 'vertical',
    'items' => '5',
  ),
    $attrs
  );
  
  // If node's page
  if (arg(0) == 'node' && is_int((int)arg(1)) && empty(arg(2)) ) {
    $_post = node_load(arg(1));
    $out = '';
    
    if ($_post->type == 'blog') {
      $ids = $res = [];
      
      // Take the values from blog post
      $post = entity_metadata_wrapper('node', $_post);
      $val_district = $post->field_district->raw();
      $val_tags = $post->field_estate_tags->raw();
      $val_ids = (string) $post->field_object_id->raw();
      
      if (!empty($val_district) || !empty($val_tags)) {
        // Prepare object for querying
        $query = new EntityFieldQuery();
        $query->entityCondition('entity_type', 'node')
          ->entityCondition('bundle', 'estate')
          ->propertyCondition('status', NODE_PUBLISHED)
          ->range(0, $attrs['items']);
        
        // District
        if (!empty($val_district)) {
          $q1 = clone $query;
          $q1->fieldCondition('field_district', 'target_id', $val_district);
          $res = $q1->execute();
        }
        
        // Tags
        if (!empty($val_tags)) {
          $q2 = clone $query;
          $q2->fieldCondition( 'field_estate_tags', 'tid', $val_tags );
          $res = drupal_array_merge_deep( $res, $q2->execute() );
        }
    
        // Convert all results above to ID's array
        if (!empty($res['node'])) {
          foreach ($res['node'] as $r) {
            $ids[] = $r->nid;
          }
        }
      }
      
      // Addup pure ID's, if there's something
      if (!empty($val_ids)) {
        $val_ids = str_replace(" ", "", $val_ids);
        $ids = array_merge($ids, explode(',', $val_ids));
      }
  
      if (!empty($text) && count($ids) > 0) {
        $out .= '<h2 class="block-title">'. $text .'</h2>';
      }
      
      $ids = array_slice($ids, 0, $attrs['items']);
      $view_args = implode(',', $ids);
      $out .= views_embed_view('search_realty', 'block_2', $view_args);
    }
  }
  
  return $out;
}

/**
 * Show tips under input field for Relevant Estate shortcode.
 *
 * @param $format
 * @param $long
 *
 * @return
 */
function estate_display_shortcode_relevant_estate_tips($format, $long) {
  $txt = 'Shortcode: [relevant_estate type=vertical items=5] Title for this block [/relevant_estate] </br>';
  $txt .= 'where <strong>type</strong> can be: horizontal or vertical; <br />';
  $txt .= '<strong>items</strong>: number of items to show.';
  
  return $txt;
}

