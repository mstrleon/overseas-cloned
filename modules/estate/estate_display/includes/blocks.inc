<?php
/**
 * @file
 * Drupal blocks related functions
 */

/**
 * Return block with country related information.
 * View with argument: root TID.
 * 
 *
 * @return mixed
 */
function estate_display_block_view_country_info() {
  $block['subject'] = $block['content'] = '';
  
  // Если термин таксономии - передаем сразу его
  if (drupal_match_path(current_path(), 'taxonomy/term/*')) {
    $tid = arg(2);
  }
  // Если страница материала - передаем связанный термин
  else if (drupal_match_path(current_path(), 'node/*')) {
    $node = node_load(arg(1));
    
    if (in_array($node->type, ['estate'])) {
      $tid = $node->field_place[LANGUAGE_NONE][0]['tid'];
    }
  }
  
  if (!empty($tid)) {
    $term = taxonomy_term_load($tid);
    
    // Выбираем только для словаря регионов
    if ($term->vid == REGION_VID) {
      $parents = taxonomy_get_parents_all($tid);
      $root = array_pop($parents);
      
      $block['subject'] = t('Useful information');
      $block['content'] = views_embed_view('country_links', 'block_1', $root->tid);
    }
  }
  
  return $block;
}