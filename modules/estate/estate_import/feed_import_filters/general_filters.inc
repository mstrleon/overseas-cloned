<?php
/**
 * Created by PhpStorm.
 * User: Chupzzz
 * Date: 07.08.2017
 */
class estateFeedImportFilter extends FeedImportFilter {
  public static function prepareFilename($field) {
    if (is_array($field)) {
      foreach ($field as &$f) {
        $f = self::prepareFilename($f);
      }
      return $field;
    }
    
    // Проверяем, что в урле не пустой файл
    if (preg_match('/([a-zA-Z0-9-]*)\.(jpg|jpeg|gif|png)(\?.*)?$/i', $field)) {
      // Очищаем дополнительные параметры, оставляем только имя файла
      $no_qs = preg_replace('/\.(jpg|jpeg|gif|png)(\?.*)?/i', '.$1', $field);
      return $no_qs;
    }
    else
      return null;
  }
  
  /**
   * Downloads and saves a file in a field
   *
   * @param mixed $field
   *   A string or an array of strings
   * @param string $path
   *   Where to save file. Default is public://
   * @param int $options
   *   Use 0 to rename existing file or 1 to replace it.
   *
   * @return mixed
   *   An object or an array of objects containing file info
   */
  public static function saveFile($field, $path = 'public://', $options = FILE_EXISTS_RENAME) {
    if (is_array($field)) {
      foreach ($field as &$f) {
        $f = self::saveFile($f, $path, $options);
      }
      return $field;
    }
    $x = 1;
    // Get file data.
    if (!@$image = file_get_contents($field))
      return NULL;
    
    $field = trim($field, '/');
    $field = drupal_substr($field, strrpos($field, '/') + 1);
    if (file_prepare_directory($path, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
      return file_save_data($image, $path . $field, (int) $options);
    }
    return NULL;
  }
  
  /**
   * This is an alis for saveFile() function.
   */
  public static function saveImage($field, $path = 'public://', $options = FILE_EXISTS_RENAME) {
    return self::saveFile($field, $path, $options);
  }
}