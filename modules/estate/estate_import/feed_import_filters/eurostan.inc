<?php
/**
 * Created by PhpStorm.
 * User: Руслан
 * Date: 12.04.2016
 * Time: 17:07
 */

function es_f_type($field) {
  $mapping = [
    1 => 1, // Appartment
    2 => 2, // House
    3 => 4, // Commercial
    4 => 4
  ];

  return !empty($mapping[$field]) ? $mapping[$field] : 1;
}

function es_f_check_district($field) {
  $map = estate_import_get_districts_map();

  if (array_key_exists($field, $map)) {
    dd($field);
    return true;
  }
  else {
    return FALSE;
  }
}
