<?php
/**
 * Файл для процессинга импорта от "СММ Черногория".
 *
 * Для правильной работы этот файл надо подключить на странице настройки импорта:
 * Edit filters --> Filter settings
 * (указать АБСОЛЮТНЫЙ путь, т.к. относительный только для папки Feed Import)
 */

function esi_montenegro_before_create_callback(array &$entity) {
  // *** ФИЛЬТР ***//
  
  // *** СВОЙСТВА ***//
  $entity['title'] = preg_replace('/^\d*\s/', '', $entity['title']) . ', ' . $entity['country'];
  
  // Проверяем значение, т.к. бывает без типа и тогда всё крашится
  if (isset($entity['type_orig'])) {
    $entity['field_estate_type'][LANGUAGE_NONE][0]['tid'] = esi_montenegro_map_estate_type($entity['type_orig']);
  }
  $entity['field_sku'][LANGUAGE_NONE][0]['value'] = 'EX-MN-' . $entity['ext_id'];
}

function esi_montenegro_before_combine_callback(array &$item, &$entity, &$changed) {
  $debug = 1;
}

function esi_montenegro_map_estate_type($in) {
  $types = esi_get_prop_lists('estate_type');
  
  $map = [
    1 => $types['house'], // Дом
    2 => $types['flat'], // Квартира
    3 => $types['land'], // Участок
    4 => $types['business'], // Коммерч
    5 => $types['business'], // Отель
    6 => $types['villa'], // Вилла
    7 => $types['townhouse'], // Таунхаус
  ];
  
  return $map[$in];
}