<?php
/**
 * Файл для процессинга импорта от "СММ Черногория".
 *
 * Для правильной работы этот файл надо подключить на странице настройки импорта:
 * Edit filters --> Filter settings
 * (указать АБСОЛЮТНЫЙ путь, т.к. относительный только для папки Feed Import)
 */

require_once(__DIR__ . '/general_filters.inc');

/**
 * Предварительная валидация загруженного объекта из XML. Самая ранняя стадия.
 *
 * Для Mouzendis проверяем возраст (не старше 3х лет) и минимальную цену (своя для каждого типа).
 *
 * @param $item
 * Чистый необработанный объект из XML.
 * @param $import
 * Объект импорта со всеми настройками.
 *
 * @return mixed
 * FALSE - если проверка текущего объекта не требуется и сам $item - если ок
 */
function fi_mouzendis__validate_raw_item($item, $import) {
  $ext_id = (int)$item->attributes()['internal-id'];
  // Нужна только продажа, никаких аренд. Тоже самое если не указан тип объекта
  if ($item->Aim != 'Продажа' || empty($item->{'property-type'})) {
    dd('SKIP ' . $ext_id . ': [1] Non sell / No type');
    return FALSE;
  }
  
  // Кипр ок ВЕСЬ, ничего не проверяем
  if ($item->Country == 'Кипр') {
    dd('PROCEED ' . $ext_id . ': is Cyprus');
    return $item;
  }
  // Фильтруем остальные страны
  else {
    // Фильтруем старьё больше 3х лет
    $date_diff = date_diff(date_create($item->{'creation-date'}), date_create('today'));
    if ($date_diff->y > 3) {
      dd('SKIP ' . $ext_id . ': [2] Too old');
      return FALSE;
    }
    
    // Ограничеваем минимальную цену для разных типов
    $cur_price = (int) $item->price->value;
    if ($cur_price) {
      $prices = [
        'Квартира'          => 75000,
        'Таунхаус'          => 80000,
        'Коттедж'           => 120000,
        'Вилла'             => 250000,
        'Земельный участок' => 150000,
        'Бизнес'            => 300000,
      ];
      
      // Берем лимит для текущего типа или дефолт
      $type = (string) $item->{'property-type'};
      $price = array_key_exists($type, $prices) ? $prices[$type] : 30000;
      
      if ($price && ($cur_price < $price)) {
        dd('SKIP ' . $ext_id . ': [3] Price is low (' . $type . ')');
        return FALSE;
      }
    }
    // Если цены вообще нет - тоже пропускаем
    else {
      dd('SKIP ' . $ext_id . ': [4] No price');
      return FALSE;
    }
    
    // Логика по типу недвижимости, но пока не надо.
    /*switch ($item->property-type) {
      case 'Квартира':
        break;
      case 'Таунхаус':
        break;
      case 'Коттедж':
        break;
      case 'Вилла':
        break;
      case 'Земельный участок':
        break;
    }*/
  }
  
  dd('PROCEED ' . $ext_id . ': Beyond the filters');
  return $item;
}


/**
 * Запускается перед СОЗДАНИЕМ новой ноды.
 *
 * @param array $entity
 */
function esi_mouzendis_before_create_callback(array &$entity) {
  // Заголовок из частей
  $entity['title'] = $entity['type_orig'] . ' ';
  
  if (!empty($entity['field_square_total'][LANGUAGE_NONE][0]['value']))
    $entity['title'] .= $entity['field_square_total'][LANGUAGE_NONE][0]['value'] . ' м<sup>2</sup>, ';
  
  $entity['title'] .= $entity['city'] . ', ' . $entity['country'];
  
  // Склеиваем доп.информацию с описанием
  if (!empty($entity['misc'])) {
    $entity['body']['und'][0]['value'] .= "\n\n" . $entity['misc'];
  }
  
  // Проверяем значение, т.к. бывает без типа и тогда всё крашится
  if (isset($entity['type_orig'])) {
    $entity['field_estate_type'][LANGUAGE_NONE][0]['tid'] = esi_mouzendis_map_estate_type($entity['type_orig']);
  }
  $entity['field_sku'][LANGUAGE_NONE][0]['value'] = 'EX-MZ-' . $entity['ext_id'];
  
  // Локация: адрес и координаты
  $entity['field_location'][LANGUAGE_NONE][0] = esi_prepare_location_field([
    'long' => $entity['geo_long'],
    'lat' => $entity['geo_lat'],
    'country' => $entity['country'],
    'city' => $entity['city'],
    'province' => $entity['region']
    ]);
  
  // Локация: страна и город (таксономия)
  $place_tid = esi_get_place_by_name($entity['country'], $entity['city']);
  $entity['field_place'][LANGUAGE_NONE][0]['tid'] = $place_tid;
  
  // Вид из окна
  $view = [];
  $view[]['value'] = empty($entity['view_city']) ? NULL : 'city';
  $view[]['value'] = empty($entity['view_sea']) ? NULL : 'sea';
  $view[]['value'] = empty($entity['view_woods']) ? NULL : 'forest';
  $view[]['value'] = empty($entity['view_mountain']) ? NULL : 'mountains';
  
  $entity['field_view'][LANGUAGE_NONE] = array_filter($view, 'esi_check_field_empty_value', ARRAY_FILTER_USE_BOTH);
  
  // Удобства
  $am_val = esi_get_prop_lists('amenities');
  $am_tids = [];
  $am_tids[]['tid'] = empty($entity['am_parking']) ? NULL : $am_val['parking'];
  $am_tids[]['tid'] = empty($entity['am_furnished']) ? NULL : $am_val['furnished'];
  $am_tids[]['tid'] = empty($entity['am_pool']) ? NULL : $am_val['pool'];
  $am_tids[]['tid'] = empty($entity['am_fireplace']) ? NULL : $am_val['fireplace'];
  
  $entity['field_amenities'][LANGUAGE_NONE] = array_filter($am_tids, 'esi_check_field_empty_tid', ARRAY_FILTER_USE_BOTH);
}

/**
 * Запускается перед ОБНОВЛЕНИЕМ существующей ноды.
 *
 * @param array $item
 * @param $entity
 * @param $changed
 */
function esi_mouzendis_before_combine_callback(array &$item, &$entity, &$changed) {
  $x = 1;
  // TODO: Здесь надо создавать/проверять те поля, которые делаем в before_create_callback()
  if (empty($item['field_place']['und'][0])) {
    if (empty($entity->field_place['und'][0]))
      unset($item['field_place']['und'][0]);
    else
      $item['field_place']['und'][0] = $entity->field_place['und'][0];
  }
}

function esi_mouzendis_map_estate_type($in) {
  $types = esi_get_prop_lists('estate_type');
  
  $map = [
    'Коттедж' => $types['house'], // Дом
    'Квартира' => $types['flat'], // Квартира
    'Земельный участок' => $types['land'], // Участок
    'Остров' => $types['land'], // Участок
    'Бизнес' => $types['business'], // Коммерч
    'Гостиница' => $types['business'], // Отель
    'Вилла' => $types['villa'], // Вилла
    'Таунхаус' => $types['townhouse'], // Таунхаус
  ];
  
  return $map[$in];
}