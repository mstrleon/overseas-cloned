<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.10';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<header id="navbar" role="banner" class="<?php print $navbar_classes; ?>">
  <div class="row_2">
    <div class="container">
    <div class="navbar-header row">
      <div class="col-secondary col-md-3" style="overflow: auto;">
        <a class="logo navbar-btn pull-left" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
        <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>

      <div class="col-main col-md-6">
        <div class="navbar-collapse collapse">
          <nav role="navigation">
            <?php if (!empty($primary_nav)): ?>
              <?php print render($primary_nav); ?>
            <?php endif; ?>
            <?php //if (!empty($secondary_nav)): ?>
              <?php //print render($secondary_nav); ?>
            <?php //endif; ?>
            <?php if (!empty($page['navigation'])): ?>
              <?php print render($page['navigation']); ?>
            <?php endif; ?>
          </nav>

          <div class="extra visible-xs-block">
            <?php echo $extrabar; ?>
          </div>
        </div>
      </div>
      <div class="extrabar col-secondary col-md-3 hidden-xs">
        <?php echo $extrabar; ?>
      </div>
    </div>
  </div>
</header>

<div class="main-container container">
  <div class="row page-title">
    <?php print render($title_prefix); ?>
    <?php if (!empty($title)): ?>
      <h1 class="page-header"><?php print $title; ?></h1>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
  </div>
  <div class="row main-content">

    <section class="content-col col-main col-md-6 <?php if (!empty($page['sidebar_second']) && !empty($page['sidebar_first'])) print "col-md-push-3 move-col"; ?>" >
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
      <a id="main-content"></a>
      <?php print $messages; ?>
      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
    </section>

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside class="second-sidebar col-secondary col-md-3 <?php if (!empty($page['sidebar_first'])) print "col-md-push-3"; ?>" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>

    <?php if (!empty($page['sidebar_first'])): ?>
      <aside class="first-sidebar col-secondary col-md-3 <?php if (!empty($page['sidebar_second'])) print  "col-md-pull-9"; ?>" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>

  </div>
</div>
<footer class="footer container">
  <div class="col-secondaryzzz col-md-8">
    <?php print render($primary_nav); ?>
  </div>
  <div class="col-secondaryzzz col-md-4">
    <h3 class="title"><?php echo date('Y'); ?> &copy; Freedom Overseas</h3>

    <div class="order pull-right hidden-xs hidden"><a href="#s" class="btn btn-default large">Оставить заявку</a></div>
    <p>
      <a href="tel:+74953745009" class="callibri_phone">+7 (495) 374-5009</a><br>
      <a href="mailto:info@freedomoverseas.ru">info@freedomoverseas.ru</a>
    </p>
    
    <div class="social">
      <a href="https://www.facebook.com/freedom.overseas/" target="_blank"><span class="icon icon-fb" aria-hidden="true"></span></a>
      <a href="https://vk.com/freedom.overseas" target="_blank"><span class="icon icon-vk" aria-hidden="true"></span></a>
      <a href="https://www.ok.ru/group/54694424805610/" target="_blank"><span class="icon icon-ok" aria-hidden="true"></span></a>
      <a href="https://twitter.com/FreedomOverseas/" target="_blank"><span class="icon icon-tw" aria-hidden="true"></span></a>
      <a href="https://www.instagram.com/freedom.overseas/" target="_blank"><span class="icon icon-ig" aria-hidden="true"></span></a>

    </div>
    
    <p class="createdby"><span style="color: #999;">Created by</span> <a href="http://sensebridgecommunications.com/" style="color: #666;">Sensebridge&nbsp;Communications</a> </p>
  </div>

  <div class="col-md-12" style="margin-bottom: 0;">
  <?php print render($page['footer']); ?>
  </div>
</footer>
