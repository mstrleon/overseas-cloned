<?php

/**
 * @file
 * template.php
 */

function estate_preprocess_html(&$vars) {
  global $language;
  
  if ($vars['is_front']) {
    drupal_add_js($GLOBALS['theme_path'] . '/js/jquery.backstretch.min.js');
  }

  drupal_add_js(['theme_path' => $GLOBALS['base_path'] . $GLOBALS['theme_path'] ] , 'setting');
  $vars['body_attributes_array']['class'][] = 'lang-' . $language->prefix;
  $vars['body_attributes_array']['class'][] = 'page-uri-' . preg_replace('/\//', '_', substr(request_uri(), 1));
}

/**
 * Override or insert variables for the page templates.
 */
function estate_preprocess_page(&$vars) {
  global $language;

//  _estate_add_backstretch_img('/' . $vars['directory'] . '/img/wg_main_bg.jpg', 'body');

  // Expanding main menu
  $menu_tree = menu_tree_all_data('main-menu');
  $vars['primary_nav'] = menu_tree_output($menu_tree);

  $vars['extrabar'] = estate_theme_extrabar($vars);
  $vars['navbar_classes_array'][1] = NULL;

  if (!empty($vars['node'])) {
    $node = $vars['node'];

    $vars['navbar_classes_array'][] = 'header-ext';
    $vars['theme_hook_suggestions'][] = 'page__node_type__' . $vars['node']->type;
    
    // TODO: старое, с предыдущего проекта. Проверить/убрать
    if ($node->type == 'estate') {
      // ***Цена для шапки
      $vars['price'] = estate_display_get_field_value('field_price', $node);

      // *** Доп.инфорация для шапки
      // Выносим в переменные, т.к. render() получает аргумент по ссылке
      $det_sq = estate_display_get_field_value('field_square_total', $node);
      $det_st = estate_display_get_field_value('field_sale_status', $node);

      $details = [
        render($det_sq),
        render($det_st)
      ];

      $vars['details'] = implode(' / ', $details);
    }

/*    if (1 == 2 && $node->type == 'estate' || $node->type == 'blog') {
      // *** Фотка в фон шапки
      $photos = !empty($node->field_photos[$language->language]) ? $node->field_photos[$language->language] : $node->field_photos[LANGUAGE_NONE];

      if (count($photos)) {
        $main_photo = $photos[0];
        $main_photo_url = convert_filepath_to_relative(image_style_url('header_bg', $main_photo['uri']));

        _estate_add_backstretch_img($main_photo_url);
      }
    }*/
  }
}

/**
 * Override default pager link.
 * We need to use HTML in text so last row was changed.
 *
 * @param $variables
 *
 * @return string
 */
function estate_pager_link($variables) {
  $text = $variables['text'];
  $page_new = $variables['page_new'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $attributes = $variables['attributes'];
  
  $page = isset($_GET['page']) ? $_GET['page'] : '';
  if ($new_page = implode(',', pager_load_array($page_new[$element], $element, explode(',', $page)))) {
    $parameters['page'] = $new_page;
  }
  
  $query = array();
  if (count($parameters)) {
    $query = drupal_get_query_parameters($parameters, array());
  }
  if ($query_pager = pager_get_query_parameters()) {
    $query = array_merge($query, $query_pager);
  }
  
  // Set each pager link title
  if (!isset($attributes['title'])) {
    static $titles = NULL;
    if (!isset($titles)) {
      $titles = array(
        t('« first') => t('Go to first page'),
        t('‹ previous') => t('Go to previous page'),
        t('next ›') => t('Go to next page'),
        t('last »') => t('Go to last page'),
      );
    }
    if (isset($titles[$text])) {
      $attributes['title'] = $titles[$text];
    }
    elseif (is_numeric($text)) {
      $attributes['title'] = t('Go to page @number', array('@number' => $text));
    }
  }
  
  // @todo l() cannot be used here, since it adds an 'active' class based on the
  //   path only (which is always the current path for pager links). Apparently,
  //   none of the pager links is active at any time - but it should still be
  //   possible to use l() here.
  // @see http://drupal.org/node/1410574
  $attributes['href'] = url($_GET['q'], array('query' => $query));
  return '<a' . drupal_attributes($attributes) . '>' . $text . '</a>';
}


function estate_preprocess_panels_pane(&$vars) {
  // Названия объектов обычно содержат HTML (например, квадратные метры) - делаем её корректной
  $s = function($title, $content) {
    return preg_replace('#(<a.*?>)(.*?)(<\/a>)#', '$1'. $title .'$3', $content);
  };
  
  $is_title_teaser = $vars['pane']->type == 'node_title' && $vars['display']->storage_id == 'node:estate:default:teaser';
  $is_title_block_like = $vars['pane']->type == 'entity_field' && $vars['display']->storage_id == 'node:estate:default:block_like' && $vars['content']['#field_name'] == 'title_field';
  
  if ($is_title_teaser || $is_title_block_like) {
    $title_orig = $vars['display']->context['panelizer']->title;
    
    if ($is_title_teaser) {
     $vars['content'] = $s($title_orig, $vars['content']);
    }
    if ($is_title_block_like) {
      $vars['content'][0]['#markup'] = $s($title_orig, $vars['content'][0]['#markup']);
    }
  }
}

function estate_preprocess_node(&$vars) {
  // Изменения в links (убран язык и пр.) - в estate_display.module
  $node = $vars['node'];
  // Подтягиваем шаблоны для тизера/полной версии и для типа-тизера/полной
  $view = $vars['teaser'] ? 'teaser' : 'full';
  $vars['theme_hook_suggestions'][] = 'node__' . $view;
  $vars['theme_hook_suggestions'][] = 'node__' . $node->type . '_' . $view;

  $vars['submitted'] = format_string('!datetime &nbsp;&nbsp;&nbsp;<span class="user">!username</span>', array('!username' => $vars['name'], '!datetime' => format_date($node->created, 'short')));

  if ($node->type == 'estate' && $view == 'full') {
    $vars['classes_array'][] = 'row';
  }
}

/**
 * Попытка пофиксить баги бутстрап-панели: филдсет не открывался по клику.
 *
 * @param $variables
 */
function estate_preprocess_bootstrap_panel(&$variables) {
  $element = &$variables['element'];
  $attributes = &$variables['attributes'];

  if (!empty($element['#group']) && $element['#collapsible'] != $variables['collapsible']) {
    $variables['collapsible'] = TRUE;
    $variables['collapsed'] = TRUE;

    if (!isset($element['#id'])) {
      $element['#id'] = drupal_html_id('bootstrap-panel');
    }

    $attributes['id'] = $element['#id'];
    $variables['target'] = '#' . $element['#id'] . ' > .collapse';
  }
}

function estate_preprocess_block(&$vars) {
  global $theme_path, $language;
//  if ($vars['block']->region == 'sidebar_first' || $vars['block']->region == 'sidebar_second') {
//    $vars['classes_array'][] = 'well';
//  }

  if ($vars['block_html_id'] == 'block-bean-front-search-text') {
    $vars['content'] = '<div class="container">' . $vars['content'] . '</div>';
  }

  // Блок-слайдер для главной. Пока отключено, но можно включить
  /*if ($vars['block_html_id'] == 'block-block-7') {
    $vars['content'] = file_get_contents(__DIR__ . '/templates/slider-front-' . $language->language . '.html');

    drupal_add_js($theme_path . '/js/jssor.slider.min.js');
    drupal_add_js($theme_path . '/js/jssor.slider.run.js');
  }*/
}

function estate_preprocess_region(&$vars) {
  // У темы сломались настройки и это самый простой способ убрать well у первой колонки
  if ($vars['region'] == 'sidebar_first') {
    if ($key = array_search('well', $vars['classes_array'])) {
      unset($vars['classes_array'][$key]);
    }
  }
}

/**
 * Implements hook_preprocess_form_element().
 * @param $variables
 */
function estate_preprocess_form_element(&$variables) {
  $element = &$variables['element'];
  $element['#wrapper_attributes'] = empty($element['#wrapper_attributes']) ? [] : $element['#wrapper_attributes'];

  // Добавляем обертку для суффикса/префикса и ставим класс, что они там есть - нужно для темизации
  if (!empty($element['#field_prefix'])) {
    $element['#field_prefix'] = '<span class="prefix">' . $element['#field_prefix'] . '</span>';
    $element['#wrapper_attributes']['class'][] = 'with-prefix';
  }
  if (!empty($element['#field_suffix'])) {
    $element['#field_suffix'] = '<span class="suffix">' . $element['#field_suffix'] . '</span>';
    $element['#wrapper_attributes']['class'][] = 'with-suffix';
  }
}

function estate_preprocess_user_picture(&$variables) {
  $variables['user_picture'] = '';
  if (variable_get('user_pictures', 0)) {
    $account = $variables['account'];
    if (!empty($account->picture)) {
      if (is_numeric($account->picture)) {
        $account->picture = file_load($account->picture);
      }
      if (!empty($account->picture->uri)) {
        $filepath = $account->picture->uri;
      }
    }
    elseif (variable_get('user_picture_default', '')) {
      $filepath = variable_get('user_picture_default', '');
    }

    if (isset($filepath)) {
      $account->type = 'user';
      $alt = estate_display_get_field_value('field_name', $account, 'user');
      $alt = $alt['#markup'];
      // If the image does not have a valid Drupal scheme (for eg. HTTP),
      // don't load image styles.
      if (module_exists('image') && file_valid_uri($filepath) && $style = variable_get('user_picture_style', '')) {
        $variables['user_picture'] = theme('image_style', array('style_name' => $style, 'path' => $filepath, 'alt' => $alt, 'title' => $alt));
      }
      else {
        $variables['user_picture'] = theme('image', array('path' => $filepath, 'alt' => $alt, 'title' => $alt));
      }
    }
  }
}

/**
 * Переопределенная таблица приложенных файлов.
 * Убраны заголовки, добавлены классы.
 *
 * @param $variables
 * @return string
 */
function estate_file_formatter_table($variables) {
//  $header = array(t('Attachment'), t('Size'));
  $header = [];
  $rows = array();
  foreach ($variables['items'] as $delta => $item) {
    $rows[] = array(
      theme('file_link', array('file' => (object) $item)),
      ['data' => format_size($item['filesize']), 'class' => 'filesize' ],
    );
  }

  return empty($rows) ? '' : theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => [ 'class' => [ 'minimal', 'no-header'] ]));
}

/**
 * Override theme_field() for node type 'Estate'.
 * Removed the colon in label.
 *
 * @param $variables
 * @return string
 */
function estate_field__estate($variables) {
  if (isset($variables['hide_field']) && !empty($variables['hide_field'])) {
    return ' ';
  }
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    // Remove the colon
//    $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . ':&nbsp;</div>';
    $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . '</div>';
  }

  // Render the items.
  $output .= '<div class="field-items"' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';

  return $output;
}

function estate_theme_extrabar($vars) {
  global $language;
  $out = '';

  if ($language->language == 'en')
    $lang = ['!short' => 'ru', '!title' => 'Русский'];
  else
    $lang = ['!short' => 'en', '!title' => 'English'];
  
  $out .= '<div class="pull-left lang-switch">';
  $out .= estate_display_get_country_select();
  
//  $out .= format_string('<a href="/?lang=!short">!title</a>', $lang);
  $out .= '</div>';

  return $out;
}

/**
 * Add bg picture to header.
 *
 * @param $path
 * @param $el
 * Selector for element.
 */
function _estate_add_backstretch_img($path, $el = '#navbar') {
  drupal_add_js(drupal_get_path('theme', 'estate') . '/js/jquery.backstretch.min.js');

  $call = "(function($) { $(document).ready(function(){ $('$el').backstretch('$path'); }) })(jQuery);";
  drupal_add_js($call, array('type' => 'inline', 'scope' => 'footer', 'weight' => 5));
}

/**
 * Bootstrap theme wrapper function for the primary menu links.
 */
function estate_menu_tree__main_menu(&$variables) {
  return '<ul class="menu nav navbar-nav">' . $variables['tree'] . '</ul>';
}
