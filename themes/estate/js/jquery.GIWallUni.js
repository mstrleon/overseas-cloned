/**
 * Created by Руслан on 01.02.2016.
 * Todo: add description
 */

(function( $ ) {
  'use strict';

  $.support.transition = (function () {

    var transitionEnd = (function () {

      var el = document.createElement('GI'),
        transEndEventNames = {
          'WebkitTransition': 'webkitTransitionEnd',
          'MozTransition': 'transitionend',
          'OTransition': 'oTransitionEnd otransitionend',
          'transition': 'transitionend'
        },
        name;

      for (name in transEndEventNames) {
        if (el.style[name] !== undefined) {
          return transEndEventNames[name];
        }
      }

    }());

    return transitionEnd && {
        end: transitionEnd
      };

  })();

  var GIWallUni = function ($el, opts) {
    // PROTECTED properties
    var self = this,
      $window = window,
      GIWall_ID = 0,
      isOpened = false,
      defaultOptions = {
        // Callbacks API
        onBeforeInit: null,
        onReady: null,
        onViewPortUpdate: null,
        onItemChange: null,
        onDestroy: null,
        onShow: null,
        onHide: null,
        onContentLoading: null,
        onContentLoaded: null,
        // Properties
        itemSelector: '.giw-el',
        defaultInlineSelector: '.giw-el-content',
        animationSpeed: 300,
        marginTop: 10,
        marginBottom: 10,
        dynamicHeight: true
      },
      istouch = 'ontouchstart' in window || window.DocumentTouch && document instanceof DocumentTouch,
      keyboardKeys = [33, 34, 35, 36, 37, 38, 39, 40, 27],
      // If Modernizr is undefined we give the priority to the javascript animations
      csstransitions = $.support.transition,
      isLoading = false,
      cachedWrapperHeight = 0,
      eventsNamespace = '.GIWallUni' + GIWall_ID,
      eventsNames = {
        click: istouch ? "touchstart" : "click",
        mousedown: istouch ? "touchstart" : "mousedown",
        mouseup: istouch ? "touchend" : "mouseup",
        mousemove: istouch ? "touchmove" : "mousemove",
        mouseleave: istouch ? "touchleave" : "mouseleave"
      },
      options = $.extend(defaultOptions, opts);

    // PUBLIC properties
    this.$el = $el;
    this.$expanderWrapper = $('<div class="GI_TW_expander"></div>');
    this.$contentPointer = $('<div class="GI_TW_pointer"></div>');
    this.$expanderInner = $('<div class="GI_TW_expander-inner"></div>');
    //this.$list = $('> ul', this.$el).eq(0);
    this.$items = $(options.itemSelector, this.$el);
    this.itemsLength = this.$items.length;
    this.currentIndex = null;
    this.currentRowIndex = null;
    this.$selected = null;
    this.prevSelected = null;
    this.selectedData = null;

    // PRIVATE METHODS
    var _init = function () {
        // append the expander html
        this.$expanderWrapper.append(this.$contentPointer);
        this.$expanderWrapper.append(this.$expanderInner);
        this.$el.prepend(this.$expanderWrapper);
        //this.$el.prepend(this.$contentPointer);

        this.bindAll();

        execCallback(options.onReady);

        GIWall_ID++;
      },

      execCallback = function (callback, arg) {
        if (typeof callback === 'function') {
          $.proxy(callback, self, arg)();
        }
      },

      _scrollTo = function (value) {
        if (!options.autoscroll) return false;
        $(options.scrollerElm || 'html,body').animate({
          scrollTop: value
        }, options.autoScrollSpeed);
      },
      /**
       * Append the class current over the active li selected
       */
      _updateCurrentClass = function () {
        self.$items.removeClass('GI_TW_Current');
        if (typeof self.currentIndex === 'number')
          self.$items.eq(self.currentIndex).addClass('GI_TW_Current');
      },

      /**
       * Append the class current over the active li selected
       */
      _updateContentPointerPosition = function () {
        this.$contentPointer.css({
          left: this.selectedData.offset.left + this.$selected.width() / 2
        });
      },
      /**
       * Load inline content into the extender inner wrapper by using a jquery selector
       * @param  { String } selector: element to select in the page
       * @return { Object } jquery deferred object
       */
      _loadInlineContent = function (selector) {
        var dfr = new $.Deferred(),
          $el = $(selector).html();

        if (!$el.length) {
          throw new Error('No element can be found using the "' + selector + '" selector');
        }

        self.$expanderInner.html($el);
        dfr.resolve();
        return dfr.promise();
      },
      /**
       * Load an image inside the extender inner wrapper
       * @param  { String } src: image url
       * @return { Object } jquery deferred object
       */
      _loadImage = function (src) {
        var dfr = new $.Deferred(),
          img = new Image();
        img.onload = function () {
          self.$expanderInner.html('<div class="GI_TW_fullimg"><img src="' + src + '" /></div>');
          dfr.resolve();
          img = null;
        };

        img.src = src;
        return dfr.promise();
      },
      /**
       * Load html contents inside the extender inner wrapper via ajax
       * @param  { String } href: contents url
       * @return { Object } jquery deferred object
       */
      _loadAjaxContents = function (href) {
        var dfr = new $.Deferred();

        $.get(href, function (data) {
          self.$expanderInner.html(data);
          dfr.resolve();
        });

        return dfr.promise();
      },
      /**
       * Update the expander wrapper height
       * @param  { Float } newHeight: height to set to the expander wrapper
       */
      _updateExpanderWrapperHeight = function (newHeight) {
        this.$expanderInner.css({
          height: newHeight
        });
        if (newHeight === cachedWrapperHeight) return;

        cachedWrapperHeight = newHeight;

        this.$expanderWrapper
          .stop(true, false)
          .addClass(csstransitions ? 'animating' : '')
          [csstransitions ? 'css' : 'animate']({
          'height': newHeight
        }, options.animationSpeed);

        //this.updateElementsPosition();
      },

      /**
       * Callback triggered anytime a slide is changed
       */
      _onItemChange = function () {
        // this.currentIndex must be always into a valid range
        if (this.selectedData.index < 0 || this.selectedData.index >= this.itemsLength) return;

        this.currentIndex = this.selectedData.index;
        this.loadInnerContents();

        execCallback(options.onItemChange, this.currentIndex);
      };

    /**
     * Bind all the events needed to work.
     */
    this.bindAll = function () {

      if (csstransitions) {
        this.$expanderWrapper.on(csstransitions.end + eventsNamespace, function () {
          $(this).removeClass('animating');
        });
      }

      /*if (options.arrows) {
        this.$el.on(eventsNames.click + eventsNamespace, '.GI_TW_arrow', this.$expanderWrapper, $.proxy(_onArrowClicked, this));
      }

      if (options.closebutton) {
        this.$el.on(eventsNames.click + eventsNamespace, '.GI_TW_close', this.$expanderWrapper, $.proxy(this.hideExpander, this));
      }*/

      this.$el.on('click' + eventsNamespace, options.itemSelector, $.proxy(this.toggleExpander, this));

      if (options.responsive)
        $window.on('resize' + eventsNamespace + ' orientationchange' + eventsNamespace, $.proxy(this.setViewport.debounce(300), this));
      if (options.keyboardNavigation)
        $window.on('keydown' + eventsNamespace, $.proxy(_onKeypress, this));

    };

    this.unbindAll = function () {
      this.$el.off(eventsNamespace);
      this.$expanderWrapper.off(eventsNamespace);
    };

    this.toggleExpander = function(e) {
      e.preventDefault();

      self.prevSelected = self.$selected;
      self.$selected = $(e.currentTarget);

      // Close if it's opened already
      if (self.$selected.length && self.$selected.hasClass('GI_TW_Current')) {
        self.hideExpander();
        return;
      }

      if (self.prevSelected) {
        if (self.$selected.offset().top != self.prevSelected.offset().top) {
          self.hideExpander(true);
          self.$expanderWrapper.one("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(e){
            self.showExpander();
          });
          return;
        }
      }

      self.showExpander();
    };

    /**
     * Open the expander div if it's closed otherwise just update the content inside
     * @param  { Object } e jQuery event object
     */
    this.showExpander = function () {
      this.selectedData = this.$selected.data();

      // if the content wrapper is already visible we just change the current content
      if (isOpened) {
        _onItemChange.call(this);
        return;
      }

      isOpened = true;

      this.$expanderWrapper.addClass('opened');

      this.setViewport();
      _onItemChange.call(this);

      execCallback(options.onShow);
    };

    /**
     * Hide the expander cleaning its inner html
     */
    this.hideExpander = function (move) {
      this.$expanderWrapper
        .removeClass('opened')
        .stop(true, false)
        [csstransitions ? 'css' : 'animate']({
        'height': 0
      }, options.animationSpeed, function(){ self.$expanderInner.empty() });

      //self.$items.css({ 'margin-bottom': options.marginBottom });

      if (move) {
        self.prevSelected.css({ 'margin-bottom': options.marginBottom });
      }
      else {
        self.$selected.css({ 'margin-bottom': options.marginBottom });
        self.$selected = null;
        self.prevSelected = null;
      }

      //this.$expanderInner.empty();
      this.currentRowIndex = null;
      this.selectedData = null;
      this.currentIndex = null;
      cachedWrapperHeight = 0;

      //this.$items.removeClass('GI_TW_Selected_Row').animate({
      //  marginBottom: options.marginBottom
      //}, options.animationSpeed);

      isOpened = false;

      _updateCurrentClass();

      //execCallback(options.onHide);
    };

    /*
     * Update the class of all the elements according to their position in the grid
     */
    this.setViewport = function () {
      if (!isOpened) return;
      // set the new offsets
      this.updateElementsPosition();

      //execCallback(options.onViewPortUpdate);
    };

    /**
     * Load the contents of the a contained into the li selected
     */
    this.loadInnerContents = function () {
      var callback,
        href = this.selectedData.href;

      isLoading = true;
      this.$expanderInner.html('<div class="GI_TW_loading"></div>');

      // Set default content wrapper for inline type
      if (!href && this.selectedData.contenttype == 'inline' && this.$selected.find(options.defaultInlineSelector).length) {
        href = this.$selected.find(options.defaultInlineSelector);
      }

      if (!href) {
        console.warn('sorry.. it was not possible to load any content');
        return;
      }
      else {
        //execCallback(options.onContentLoading);
        switch (this.selectedData.contenttype) {
          case 'ajax':
            callback = _loadAjaxContents(href);
            break;
          case 'inline':
            callback = _loadInlineContent(href);
            break;
          default:
            callback = _loadImage(href);
            break;
        }

        callback.then(function () {
          // set this value temporary to auto
          self.$expanderInner.css({
            height: 'auto'
          });

          var newHeight = options.dynamicHeight ? self.$expanderInner.outerHeight() : options.initialWrapperHeight;
          _updateExpanderWrapperHeight.call(self, newHeight);

          // update the DOM
          self.update();
          _scrollTo(self.$expanderWrapper.offset().top - options.scrollOffset);
          //execCallback(options.onContentLoaded);
          isLoading = false;
        });
      }
    };

    /*
     * Update the plugin DOM elements
     */
    this.update = function () {
      this.selectedData = this.$selected.data();

      if (options.arrows) {
        _updateArrows();
      }

      _updateCurrentClass.call(this);
      this.updateElementsPosition();

      this.currentRowIndex = this.selectedData.rowIndex;
    };

    /**
     * Update the elements position
     */
    this.updateElementsPosition = function () {
      this.setOffsets();
      this.updateExpanderPosition();
      _updateContentPointerPosition.call(this);
    };

    this.setOffsets = function () {
      // these variables will be used inside the loop the get the li row position
      var cachedOffsetTop = 0,
        rowIndex = 0,
        $previousLi;
      // loop all the lis in the grid
      this.$items.each($.proxy(function (i, elm) {
        var $item = $(elm),
          itemData = $item.data();
        // remove all the the wall classes
        $item.removeClass('GI_TW_First GI_TW_Last GI_TW_Index-' + itemData.index + ' GI_TW_Row-' + itemData.rowIndex);
        // check whether this li is in the current viewport

        // update the classes only of the visible lis
        var isFirst = false,
          tmpOffset = $item.position(),
          liPosition = {
            top: ~~tmpOffset.top,
            left: ~~tmpOffset.left
          };
        // increase by one row if the element has an offset different from the previous one
        // considering images having the same offset only the ones having an offset gap smaller than 6px (to fix a webkit weird behavior)
        /*if (liPosition.top >= cachedOffsetTop + 3 || liPosition.top <= cachedOffsetTop - 3) {
          if ($previousLi)
            $previousLi.addClass('GI_TW_Last');
          isFirst = true;
          rowIndex++;
        }
*/        // add the needed classes to detect the li inside the grid
        $item.addClass((isFirst ? 'GI_TW_First ' : ' ') + 'GI_TW_Index-' + i + ' GI_TW_Row-' + rowIndex);
        // store the wall data
        $item.data({
          rowIndex: rowIndex,
          offset: liPosition,
          index: i
        });
        $previousLi = $item;
        cachedOffsetTop = liPosition.top;

      }, this));
    };

    this.updateExpanderPosition = function () {
      if (!isOpened) return;
      // set expandWrapper top position
      var newTopPosition = this.selectedData.offset.top + this.$selected.height() + options.marginTop;
      this.$expanderWrapper.css({ top: newTopPosition });

      // set active element margin
      var marginGap = options.marginTop + cachedWrapperHeight + options.marginBottom;


      this.$selected.stop(true, false)[csstransitions ? 'css' : 'animate']({
        'margin-bottom': marginGap
      }, options.animationSpeed);

      this.$selected.on("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){
        if(self.prevSelected) {
          self.prevSelected.stop(true, false)[csstransitions ? 'css' : 'animate']({
            'margin-bottom': options.marginBottom
          }, options.animationSpeed);
        }
      });

    };

    /**
     * Resize the height of the expander to any custom value
     * @param  { Int } newHeight: the new height value that must be set to the expander wrapper
     */
    this.resizeHeight = function (newHeight) {
      _updateExpanderWrapperHeight.call(this, newHeight);
      this.setViewport();
    };

    /**
     *
     * Show the content of any brick by selecting it via index
     * @param { Int }
     *
     */
    this.showItemByIndex = function (index) {
      var $currentLi = this.$items.eq(index);
      if ($currentLi.length) {
        self.$selected = $currentLi;
        self.selectedData = $currentLi.data();
        self.showExpander();
      }
    };

    _init.call(this);
    return this;
  };

  /***
   * Add plugin to jQuery.
   */
  $.fn.GIWallUni = function (opts) {
    if (!this.length) {
      return;
    }
    return new GIWallUni(this, opts);
  };

}( jQuery ));


// TODO: вынести в script.js
jQuery(document).on('ready', function() {
  giw = jQuery('.giw-container').GIWallUni({
    marginBottom: 10,
    onItemChange: function () {
      jQuery('.giw-content .row > div', this.$expanderInner).matchHeight();
    },
    onReady: function() {
      this.showItemByIndex(1);
    }
  });

  jQuery().prettyEmbed({ useFitVids: true });

  // Cкрываем подпись у встроенного видео
  jQuery('.pretty-embed').on('click', function(e){
    jQuery(this).parent().find('.title').hide();
  })
});