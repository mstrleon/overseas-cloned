jQuery(document).ready(function ($) {
  var jssor_1_SlideoTransitions = [
    [{b:0.0,d:1000.0,x:640.0,e:{x:27.0}}]
  ];

  var jssor_1_options = {
    $AutoPlay: true,
    $Idle: 5000,
    $SlideWidth: $('#jssor_1').parent().width(),
    $CaptionSliderOptions: {
      $Class: $JssorCaptionSlideo$,
      $Transitions: jssor_1_SlideoTransitions
    },
    $ArrowNavigatorOptions: {
      $Class: $JssorArrowNavigator$
    },
    $BulletNavigatorOptions: {
      $Class: $JssorBulletNavigator$
    }
  };

  var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

  //responsive code begin
  //you can remove responsive code if you don't want the slider scales while window resizing
  function ScaleSlider() {
    var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
    // Dirty fix for IE and some mobiles
    $('#jssor_1, #jssor_1 > div').width(refSize);

    if (refSize) {
      refSize = Math.min(refSize, 700);
      jssor_1_slider.$ScaleWidth(refSize);
    }
    else {
      window.setTimeout(ScaleSlider, 100);
    }
  }
  ScaleSlider();
  $(window).bind("load", ScaleSlider);
  $(window).bind("resize", ScaleSlider);
  $(window).bind("orientationchange", ScaleSlider);
  //responsive code end
});

function getRows(selector) {
  var height = jQuery(selector).height();
  var line_height = jQuery(selector).css('line-height');
  line_height = parseFloat(line_height)
  var rows = height / line_height;
  return Math.round(rows);
}