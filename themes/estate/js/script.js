(function ($) {
  $(document).ready(function () {
    // Перекидываем блок инвайтов в модал бутстрапа
    $('#block-invite-invite .block-title').appendTo('#modal-invite .modal-header');
    $('#block-invite-invite').appendTo('#modal-invite .modal-body');
    $('#modal-invite .btn-close').appendTo('#invite-form > div');

    // Панели плиткой на странице списков
    $('.masonry').masonry({
      itemSelector:    '.grid',
      columnWidth:     '.grid',
      percentPosition: true
    });

    bind_analytics_events();
    init_front_filter();
    init_sticky_sidebar();
  });

  /**
   * Биндим цели GA/Метрики на какие-то действия.
   */
  function bind_analytics_events() {
    // Отправка формы заявки
    // TODO: дописать конкретные классы!
    $(".webform-client-form").on('submit', function() {
      yaCounter45773136.reachGoal('FORMA');
      ga ('send', 'event', 'forms', 'successfully');
    });
  }

  function init_front_filter() {
    var $p = $('.front .pane-front-about-search');

    if ($p.length) {
      // Делаем блок с фильтром шириной на 100%
      var margin = ($(window).width() - $p.width()) / 2;
      $p.css('margin', '0 -' + margin + 'px');

      // Добавляем ширину контейнера вложенному элементу
      $p.find(' > div').addClass('container');

      // Подключаем слайдшоу в фон
      var images = [
        Drupal.settings.theme_path + "/img/front-gall/front-bg1.jpg",
        Drupal.settings.theme_path + "/img/front-gall/front-bg2.jpg",
        Drupal.settings.theme_path + "/img/front-gall/front-bg3.jpg",
        Drupal.settings.theme_path + "/img/front-gall/front-bg4.jpg",
        Drupal.settings.theme_path + "/img/front-gall/front-bg5.jpg"
      ];

      $p.backstretch(images, {
        duration: 12000,
        fade: 2500
      });
    }
  }

  function init_sticky_sidebar() {
    // Включаем Affix только для планшеты+
    var mq = window.matchMedia( "(min-width: 768px)" );

    if (mq.matches) {
      console.warn('AFFIX IS ON');

      var $content_col = $('.content-col.col-main');
      var $sidebar2 = $('.region-sidebar-second');

      // Если центральная колонка больше сайдбара
      if ($content_col.height() > $sidebar2.height()) {
        $sidebar2.affix({
          offset: {
            top: $('.row.main-content').offset().top,
            bottom: $('footer').outerHeight(true) // Но в идеале, считать по низу .row.main-content
          }
        });
      }

    } else {
      // MOBILE
    }
  }
})(jQuery);
