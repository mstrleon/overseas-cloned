module.exports = function (grunt) {
  grunt.initConfig({
    stylus: {
      compile: {
        options: {
          compress: true,
          'include css': true,
          sourcemap: {
            inline: true // enable
            //comment:false // disable
          }
        },
        files: {
			'css/custom.css': 'stylus/custom.styl'
		}
// 'css/custom.css': 'stylus/custom.styl'
      }
    },

    watch: {
      stylus: {
        files: 'stylus/**/*.styl',
        tasks: ['stylus:compile']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-stylus');

  grunt.registerTask('default', ['stylus:compile', 'watch']);
};

