If using Method 1, this folder is where the output from the compiled LESS files
should be generated. If using the LESS module, this folder can be ignored or
removed.

If using Method 2, edit the style.css file to your liking.

*************** BnbClub
style.css - собирается из less бутстрапа, там все настройки в variables.less и прочие бонусы.
custom.css - все наши кастомные стили, генерятся из Stylus. Если надо что-то поправить, правь исходник, а не css!
